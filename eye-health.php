<!-- header -->
<?php
$title = 'Eye-health';
include 'includes/header.php';
?>


<!-- body -->
<section>
  <div class="eye-health_cover">
    <div class="container">
      <div class="part13">
        <div class="row">
          <div class="col-md-6 col-sm-12 eye-health_para1">
            <div class="content6 slideInLeft animated" data-animate="slideInLeft" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <h2>Eye Health</h2>
              <p>To maintain good eye health and vision, it is important to have your eyes tested every two years or more frequently if advised.</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <img src="images/eye-health_bnr.png" alt="">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 eye-health_para2">
            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <p>You should seek advice immediately if you have any sudden loss of vision, eye pain, inflammation or irritation.Most eye diseases can be found during a routine eye test and treated when detected at an earyly stage. If you have health problemsm, such as diabetes, you may need to visit your optometrist more frequently to detect any complications.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="part12">
      <p>Eye Health Services</p>
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyetest_modal">
          <img src="images/box1.jpg" alt="">
          <p>Eyetest</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#children_modal">
          <img src="images/box2.jpg" alt="">
          <p>Children Eyetest</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecondition_modal">
          <img src="images/box3.jpg" alt="">
          <p>Eye Conditions</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecare_modal">
          <img src="images/box4.jpg" alt="">
          <p>Care for your eye</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row part5">
      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
        <div class="frames_img">
          <img src="images/frames.png" alt="" class="frames_style">
          <div class="content5 fadeIn animated" data-animate="fadeIn" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
            <h2>Frames</h2>
            <p>we'll help you find the perfect Frames</p>
            <a href="frames.php">Find out more<i class="fa fa-chevron-right" style="padding-left:20px;"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row part6">
      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
        <div class="contactlens_img">
          <img src="images/contactlens.png" alt="" class="contact_style">
          <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
            <h2>Contact Lenses</h2>
            <p>we'll help you find the perfect Frames</p>
            <a href="contact-lenses.php">Find out more<i class="fa fa-chevron-right" style="padding-left:20px;"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row part7">
      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
        <div class="contactlens_img">
          <img src="images/aboutus.png" alt="" class="contact_style fadeIn animated" data-animate="fadeIn" data-duration="2.5s">
          <div class="content7 fadeInUp animated" data-animate="fadeInUp" data-duration="1.5s" style="animation-duration: 1.5s; visibility: visible;">
            <h1>iCare Vision Specialists is an exclusive</br>optical showroom located in heart of</br>Conventry</h1>
            <a href="#">Know more<i class="fa fa-chevron-right" style="padding-left:28px;"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- horizontal line -->
<div class="horizontal_line1">
  <hr>
</div>

<section>
  <div class="container">
    <div class="row part8">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="text-center">
          <p>Brands we sell</p>
          <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel1">
            <div class="item">
              <img src="images/essilor1.png" alt="">
            </div>
            <div class="item">
            <img src="images/crizal2.png" alt="">
            </div>
            <div class="item">
              <img src="images/hoya1.png" alt="">
            </div>
            <div class="item">
              <img src="images/transitions1.png" alt="">
            </div>
            <div class="item">
              <img src="images/zeiss2.png" alt="">
            </div>
            <div class="item">
              <img src="images/cibavision1.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- footer -->
<?php include 'includes/footer.php'; ?>
