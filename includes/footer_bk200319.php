<footer>
  <div class="container">
    <!-- first row -->
    <div class="row">
      <div class="col-md-3 col-sm-12">
        <ul>
          <li><h3>iCare Vision</h3></li>
          <li><a href="#" target="_self">What we do</a></li>
          <li><a href="#" target="_self">iCare NHS Guide</a></li>
          <li><a href="contact-us.php" target="_self">Contact us</a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-12 foot_info">
        <ul>
          <li><h3>Information</h3></li>
          <li><a href="#" target="_self">Frames</a></li>
          <li><a href="#" target="_self">Contact Lenses</a></li>
          <li><a href="#" target="_self">Eye Care Health</a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-12 foot_social">
        <ul>
          <li><h3>Get Social</h3></li>
          <li><i class="fa fa-facebook-square"></i><a href="https://www.facebook.com/" target="_blank">Facebook</a></li>
          <li><i class="fa fa-twitter-square"></i><a href="https://twitter.com/" target="_blank">Twitter</a></li>
          <li><i class="fa fa-instagram"></i><a href="https://www.instagram.com/" target="_blank">Instagram</a></li>
        </ul>
      </div>
    </div>
    <!-- second row -->
    <div class="row">
      <div class="col-lg-4 col-md-7 col-sm-12 foot_rights">
        <p>&#169; 2019 iCare Vision Specialists. All Rights Reserved</p>
      </div>
      <!-- <div class="col-md-1"></div> -->
      <div class="col-lg-3 col-md-4 col-sm-12 foot_policy">
        <p>Disclaimer . <a href="#"> Privacy Policy</a></p>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 foot_design">
        <p>Web Design by Quick Innovation</p>
      </div>
    </div>
  </div>
</footer>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script>
  $('.carousel1').owlCarousel({
    loop:true,
		margin:10,
		nav:false,
		dots:true,
		responsiveClass:true,
		autoplay:true,
        autoplayTimeout:3000,
		responsive:{
      0:{
        items:1,
        dots:false,
      },
      600:{
        items:1,
      },
      1000:{
        items:1
      }
    }
  });
  $('.testi').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:true,
		responsiveClass:true,
		// autoplay:true,
    // autoplayTimeout:3600,
    responsive:{
      0:{
        items:1,
      },
      600:{
        items:1,
      },
      1000:{
        items:1
      }
	   }
   });
</script>
<script>
  $('.animated').scrolla();
</script>
