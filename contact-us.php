<!-- header -->
<?php
$title = 'Contact Us';
include 'includes/header.php'; ?>
<?php
@session_start();

if(@$_POST['submit'] == 'SUBMIT'){

			$fullname=@$_POST['fullname'];
			$contact=@$_POST['contact'];
			$email=@$_POST['email'];
			$sub=@$_POST['query'];


			$subject_text = 'contact  Form';

    	// $insert_contacat =$contact_obj->insert_contact(@$_POST);

			$subject=$subject_text;

			$to = 'kiran@blueitservices.net';

			$from = $email;

			$headers = "From: ". strip_tags($to) . "\r\n";

			$headers .= "Reply-To: ". strip_tags($to) . "\r\n";

			$headers .= "MIME-Version: 1.0\r\n";

			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

			$mailbody="<table width='1000' border='0' cellspacing='0' cellpadding='0'>";

			$mailbody=$mailbody."<th>iCare Vision Enquiry Form</th>";


			$mailbody=$mailbody."<tr><td width='191' height='29'><b> Name :</b></td><td width='519'> ".$fullname." </td></tr>";
      $mailbody=$mailbody."<tr><td width='191' height='29'><b> Email :</b></td><td width='519'> ".$email." </td></tr>";
      $mailbody=$mailbody."<tr><td width='191' height='29'><b>Phone Number :</b></td><td width='519'> ".$contact." </td></tr>";
			$mailbody=$mailbody."<tr><td width='191' height='29'><b> Enquiry :</b></td><td width='519'> ".$sub." </td></tr>";


			$mailbody=$mailbody."</table>";

			$mailbody1="<table width='1000' border='0' cellspacing='0' cellpadding='0'>";

			$mailbody11=$mailbody1."<th>Enquiry Form</th>";


			$mailbody1=$mailbody1."<tr><td width='919'><b> Thank You For Submitting Your Details. We Will Get in Touch With You Soon.</b> </td></tr>";
			$mailbody1=$mailbody1."<tr><td width='519'></td></tr>";
			$mailbody1=$mailbody1."<tr><td width='519'></td></tr>";
			$mailbody1=$mailbody1."<tr><td width='519'><b> Thanks & Regards</b> </td></tr>";
			$mailbody1=$mailbody1."<tr><td width='519'><b>iCare Vision Specialists</b> </td></tr>";
			$mailbody1=$mailbody1."<tr><td width='519'><b>02476 442 272</b></td></tr>";
			$mailbody1=$mailbody1."<tr><td width='519'><b></b></td></tr>";
			$mailbody1=$mailbody1."<tr><td width='519'><b></b></td></tr>";
			$mailbody1=$mailbody1."<tr><td width='519'><b></b></td></tr>";
			$mailbody1=$mailbody1."<tr><td width='519'><b></b></td></tr>";

			$mailbody1=$mailbody1."</table>";

			@mail($to,$subject,$mailbody,$headers);
			@mail($from,$subject,$mailbody1,$headers);
			echo "<script>location.replace('CONTACT.php?msg=succ');</script>";
	}
?>

<!-- body -->
<!-- part1 -->
<section>
  <div class="contact_part1">
    <img src="images/contact_cover.jpg" alt="">
    <p>Book an <span style="color:#ca0e10;">Eyetest</span><a href="#" onclick="openEyetest()" data-target="#myslide" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">Click here <i class="fa fa-chevron-right"></i></a></p>
  </div>
</section>

<!-- part2 -->
<section>
  <div class="container contact_layout">
    <div class="row contact_part2">
      <div class="col-md-6 col-sm-12 box1">
        <div class="pulse animated" data-animate="pulse" data-duration="3.0s" style="animation-duration: 3s; visibility: visible;">
          <h2 style="">ContactUs</h2>
          <table border="0" width="100%" style="font-size:13px;font-weight:500;" cellpadding="5">
            <tr>
              <td rowspan="2" style="font-size:16px;color: #ca0e10;text-align:center"><i class="fas fa-clock"></i>Opening Hours</td>
              <td>Mon - Sat</td>
              <td>9am - 5:30pm</td>
            </tr>
            <tr>
              <td>Closed for lunch</td>
              <td>1pm - 2pm</td>
            </tr>
            <tr>
              <td></td>
            </tr>
            <tr>
              <td style="font-size:32px;text-align:center"><i class="fa fa-map-marker" style="color:#ca0e10;"></i></td>
              <td>	291 - 293 Walsgrave Rd </br>Coventry CV2 4BE</td>
            </tr>
            <tr>
              <td></td>
            </tr>
            <tr>
              <td style="font-size:26px;text-align:center"><i class="fa fa-phone" style="color:#ca0e10;"></i></td>
              <td>02476 442 272</td>
            </tr>
            <tr>
              <td style="font-size:26px;text-align:center"><i class="fa fa-envelope-open" style="color:#ca0e10;"></i></td>
              <td>info@icare-vs.co.uk</td>
            </tr>
          </table>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 box2" style="">
        <div class="contact_content1 pulse animated" data-animate="pulse" data-duration="3.0s" style="animation-duration: 3s; visibility: visible;">
          <h4 class="text-center mb-3" style="color:#ca0e10;">Our staff are always ready to help and we will respond to your query as soon as possible.</h4>
          <form action="#" method="post" role="form" onsubmit="return contact_validation();">
            <div class="row form_style">
              <div class="col-md-6 col-sm-12">
                <input type="text" name="fullname" value="" id="fullname" class="form-control" placeholder="Full Name" onblur="return contact_validation();">
                <span id="fullname_error"></span>
              </div>
              <div class="col-md-6 col-sm-12">
                <input type="tel" name="contact" value="" id="contact_number" class="form-control" placeholder="Contact Number" onblur="return contact_validation();">
                <span id="contact_number_error"></span>
              </div>
            </div>
            <div class="row form_style">
              <div class="col-md-12 col-sm-12">
                <input type="email" name="email" value="" id="contact_email" class="form-control" placeholder="Email Address" onblur="return contact_validation();">
                <span id="contactemail_error"></span>
              </div>
              <div class="col-md-6 col-sm-12">
              </div>
            </div>
            <div class="row form_style">
              <div class="col-md-12 col-sm-12">
                <textarea rows="4" cols="0" name="query" id="contact_querybox" class="form-control" placeholder="Write your query here..." onblur="return contact_validation();"></textarea>
                <span id="contact_querybox_error"></span>
              </div>
            </div>
            <div class="row form_style">
              <div class="col-md-12 col-sm-12">
                <button href="#" type="submit" class="btn3" name="submit" id="" value="submit">Submit<i class="fa fa-arrow-right" style="padding-left:30px;"></i></button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <hr>
    <div class="row contact_part2">
      <div class="col-md-12 col-sm-12">
        <section>
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d27534.478564292716!2d-1.4807939834777795!3d52.41162266285492!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48774bdec6c462a9%3A0xdd66c3a5ac2fcf89!2s291+Walsgrave+Rd%2C+Coventry+CV2+4AX%2C+UK!5e0!3m2!1sen!2sus!4v1550833541755" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
        </section>
      </div>
    </div>
  </div>
</section>

<!-- footer -->
<?php include 'includes/footer.php'; ?>
