<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <!-- company logo -->
    <link rel="icon" type="image/png" href="images/logo.png">
    <!-- custom styles -->
    <link rel="stylesheet" href="css/style.css">
    <!-- media query -->
    <link rel="stylesheet" href="css/media.css">
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <!-- bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- font-awesome -->
    <link rel="stylesheet" href="node_modules/css/font-awesome.css">
    <!-- owl-carousel -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- owl-carousel theme -->
  	<link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!-- animations -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- calendar -->
    <link rel="stylesheet" href="pg-calendar/dist/css/pignose.calendar.min.css">
    <!-- jquery -->
    <script src="js/jquery.min.js"></script>
    <!-- scrolla animation -->
    <script src="js/scrolla.jquery.min.js"></script>
    <!-- calendar script -->
    <script src="pg-calendar/dist/js/pignose.calendar.min.js"></script>
    <script src="pg-calendar/dist/js/pignose.calendar.full.min.js"></script>
  </head>
  <body>
    <div class="container">
      <div class="row header_cont">
        <div class="col-md-5">
          <div class="">
            <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook-square"></i></a>
            <a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter-square"></i></a>
            <a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
          </div>
          <hr>
          <div class="nav_links_left">
            <a href="index.php" target="_self">Home</a>
            <a href="what-we-do.php" target="_self">What we do</a>
            <a href="contact-us.php" target="_self">Contact Us</a>
          </div>
        </div>
        <div class="col-md-2">
          <img src="images/logo.png" alt="">
        </div>
        <div class="col-md-5 text-right">
          <div class="">
            <a href="#" class="appointment_link" target="_self">Book an appointment<i class="fa fa-chevron-right"></i></a>
          </div>
          <hr>
          <div class="nav_links_right">
            <a href="#" target="_self">Frames</a>
            <a href="#" target="_self">Contact Lenses</a>
            <a href="eye-health.php" target="_self">Eye Health</a>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
