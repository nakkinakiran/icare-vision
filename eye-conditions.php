<!-- header -->

<?php

  $title = 'Eye conditions';

  include 'includes/header.php';

?>



<!-- body -->

<section>

  <div class="eye-test_cover">

    <div class="container">

      <div class="part14">

        <div class="row">

          <div class="col-lg-6 col-md-12 col-sm-12">

            <div class="content6 slideInLeft animated" data-animate="slideInLeft" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">

              <h2>Eye Conditions</h2>

              <p>When you have a problem with your eyes, you know you can turn to us for help.</p>

            </div>

          </div>

          <div class="col-lg-6 col-md-12 col-sm-12 content_img">

            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">

              <img src="images/eye-conditions.png" alt="">

            </div>

          </div>

        </div>

        <div class="row">

          <div class="col-md-12 col-sm-12 eye-test_para2">

            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">

              <p>Leading the way with new medications and surgical procedures, Primary Eye Care can diagnose and treat a wide range of diseases and conditions. Advances in technology have impacted all aspects of vision care. Surgical procedures that once required a hospital stay and lengthy recovery are now performed by our doctors on an outpatient basis, with most patients returning to their normal activities within hours.</p>

              <p>We continually invest in the most state-of-the-art diagnostic and surgical equipment. And our doctors go through rigorous, ongoing training to ensure that we’re doing everything we can to improve our patients’ vision and quality of life. We’re committed to providing the most advanced care available.</p>

              <h2 style="font-size: 30px;">Treatments</h2>

              <p>You know your eyes better than anyone. If they don’t see, feel or look as well as you’d like them to, call us.</p>

            </div>

          </div>

        </div>

        <div class="row" style="padding-top:40px">

          <div class="col-lg-6 col-md-12 col-sm-12 content_img">

            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3s" style="animation-duration: 2s; visibility: visible;">

              <img src="images/treatments.png" alt="">

            </div>

          </div>

          <div class="col-lg-6 col-md-12 col-sm-12">

            <div class="content7 bounceInRight animated" data-animate="bounceInRight" data-duration="2.5s" style="animation-duration: 1.5s; visibility: visible;">

              <table border="0" cellpadding="10" width="100%">

                <tr>

                  <td>Astigmatism</td>

                  <td>Blepharitis</td>

                </tr>

                <tr>

                  <td>Cataracts</td>

                  <td>Conjunctivitis (Pink-Eye)</td>

                </tr>

                <tr>

                  <td>Corneal Abrasions</td>

                  <td>Corneal Disease</td>

                </tr>

                <tr>

                  <td>Corneal Ulcers</td>

                  <td>Diabetic Retinopathy</td>

                </tr>

                <tr>

                  <td>Dry Eye</td>

                  <td>Farsightedness</td>

                </tr>

                <tr>

                  <td>Flashes & Floaters</td>

                  <td>Fuchs Dystrophy</td>

                </tr>

                <tr>

                  <td>Keratoconus</td>

                  <td>Low Vision</td>

                </tr>

                <tr>

                  <td>Macular Degeneration</td>

                  <td>Monovision</td>

                </tr>

                <tr>

                  <td>Narrow-Angle Glaucoma</td>

                  <td>Normal Vision</td>

                </tr>

                <tr>

                  <td>Nearsightedness</td>

                  <td>Open-Angle Glaucoma</td>

                </tr>

                <tr>

                  <td>Presbyopia</td>

                  <td>Pterygium</td>

                </tr>

                <tr>

                  <td>Retinal Detachment</td>

                  <td>Retinal Vein Occlusion</td>

                </tr>

                <tr>

                  <td>Strabismus</td>

                  <td>Uveitis</td>

                </tr>

              </table>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<section>

  <div class="container">

    <div class="part12">

      <div class="row">

        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyeservice_modal">

          <h1 class="box_text">Eye Health Services</h1>

        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#children_modal">

          <img src="images/box2.jpg" alt="">

          <p>Children Eyetest</p>

        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecondition_modal">

          <img src="images/box3.jpg" alt="">

          <p>Eye Conditions</p>

        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecare_modal">

          <img src="images/box4.jpg" alt="">

          <p>Care for your eye</p>

        </div>

      </div>

    </div>

  </div>

</section>



<!-- horizontal line -->

<div class="horizontal_line1">

  <hr>

</div>



<section>

  <div class="container part2">

    <div class="row d-flex justify-content-center">

      <div class="col-lg-1"></div>

      <div class="col-lg-2 col-md-12 col-sm-12">

        <img src="images/nhs.png" alt="" width="125px" height="50px" class="nhs_icon">

      </div>

      <div class="col-lg-6 col-md-10 col-sm-10 test_content">

        <span>Do you qualify for a <b>Free Eye Test?</b></span></br>

        <p>Find out if you're entitled to a free NHS-funded Eye Test and optical vounchers.</p>

      </div>

      <div class="col-lg-1 col-md-2 col-sm-2">

        <a href="#" target="_self"><i class="fa fa-chevron-right"></i></a>

      </div>

    </div>

  </div>

</section>



<section>

  <div class="container">

    <div class="part15">

      <div class="content7 fadeIn animated" data-animate="fadeIn" data-duration="3s" style="animation-duration: 1.5s; visibility: visible;">

        <div class="book-eye-test_cover">

          <div class="row book-eye-test_content">

            <div class="col-lg-5 col-md-12 col-sm-12 ">

              <div class="content7 slideInLeft animated" data-animate="slideInLeft" data-duration="2s" style="animation-duration: 1.5s; visibility: visible;">

                <img src="images/eye-test_icon.png" alt="">

              </div>

            </div>

            <div class="col-lg-7 col-md-12 col-sm-12">

              <div class="content7 slideInRight animated" data-animate="slideInRight" data-duration="2s" style="animation-duration: 1.5s; visibility: visible;">

                <h3>Book an eye test today.</h3>

                <p><input type="text" name="" value="" placeholder="Postal Code"><a href="">Start Now</a></p>

                <p>or Call us for appointment <b style="margin-left: 10px;font-size:17px">024 7644 2272</b></p>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<section>

  <div class="container">

    <div class="row part7">

      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">

        <div class="contactlens_img">

          <img src="images/aboutus.png" alt="" class="contact_style fadeIn animated" data-animate="fadeIn" data-duration="2.5s">

          <div class="content7 fadeInUp animated" data-animate="fadeInUp" data-duration="1.5s" style="animation-duration: 1.5s; visibility: visible;">

            <h1>iCare Vision Specialists is an exclusive</br>optical showroom located in heart of</br>Conventry</h1></br>

            <a href="#">Know more<i class="fa fa-chevron-right" style="padding-left:28px;"></i></a>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<!-- horizontal line -->

<div class="horizontal_line1">

  <hr>

</div>



<section>

  <div class="container">

    <div class="row part8">

      <div class="col-lg-12 col-md-12 col-sm-12">

        <div class="text-center">

          <p>Brands we sell</p>

          <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel1">

            <div class="item">

              <img src="images/essilor1.png" alt="">

            </div>

            <div class="item">

            <img src="images/crizal2.png" alt="">

            </div>

            <div class="item">

              <img src="images/hoya1.png" alt="">

            </div>

            <div class="item">

              <img src="images/transitions1.png" alt="">

            </div>

            <div class="item">

              <img src="images/zeiss2.png" alt="">

            </div>

            <div class="item">

              <img src="images/cibavision1.png" alt="">

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<!-- footer -->

<?php include 'includes/footer.php'; ?>

