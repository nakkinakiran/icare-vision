<!-- header -->
<?php
$title = 'What-we-do';
include 'includes/header.php'; ?>


<!-- body -->
<section>
  <div class="what-we-do_cover">
    <div class="container">
      <div class="part11">
        <div class="row">
          <div class="col-md-6 col-sm-12 what-we-do_para1">
            <div class="content6 slideInLeft animated" data-animate="slideInLeft" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <h2>What we do</h2>
              <p>Our professional team of optometrists have been working for over 25 years ful-filling this mission by offering a great and highly regarded service.</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <img src="images/what-we-do_box1.png" alt="">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 what-we-do_para2">
            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <p>We believe that our high quality eyecare translates into high quality eyeware, ensuring that our patients are treated and cared for exactly
              the same way we would our closest relatives. Our extensive range of spectacles and sunglasses is always evolving, with new and exciting brands constantly being reviewed and added to our portfolio. The current range includes Givenchy, Police,Ted Baker, Gant, Diesel, Jaeger, Black,
              Dominance, Pepe Jeans, Guess, Hackett, Stepper, Replay, Converse, Sketchers and more. We are confident that there will be something for everyone of our patients
              coupled with our guarantee to offer the best possible price.</p>
              <p>Our showroom is equipped with state of-the-art eye-testing facilities. Qualified staff assists customers in choosing the perfect spectacles for their eyes. The presence of a wide selection of lenses, from an eco to premium range, promises to satisfy every taste and budget. iCare has made spectacle shopping in conventry a pleasurable experience.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="part12">
      <p>Eye Health Services</p>
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyetest_modal">
          <img src="images/box1.jpg" alt="">
          <p>Eyetest</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#children_modal">
          <img src="images/box2.jpg" alt="">
          <p>Children Eyetest</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecondition_modal">
          <img src="images/box3.jpg" alt="">
          <p>Eye Conditions</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecare_modal">
          <img src="images/box4.jpg" alt="">
          <p>Care for your eye</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row part7">
      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
        <div class="contactlens_img">
          <img src="images/aboutus.png" alt="" class="contact_style fadeIn animated" data-animate="fadeIn" data-duration="2.5s">
          <div class="content7 fadeInUp animated" data-animate="fadeInUp" data-duration="1.5s" style="animation-duration: 1.5s; visibility: visible;">
            <h1>iCare Vision Specialists is an exclusive</br>optical showroom located in heart of</br>Conventry</h1>
            <a href="#">Know more<i class="fa fa-chevron-right" style="padding-left:28px;"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- horizontal line -->
<div class="horizontal_line1">
  <hr>
</div>

<section>
  <div class="container">
    <div class="row part8">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="text-center">
          <p>Brands we sell</p>
          <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel1">
            <div class="item">
              <img src="images/essilor1.png" alt="">
            </div>
            <div class="item">
            <img src="images/crizal2.png" alt="">
            </div>
            <div class="item">
              <img src="images/hoya1.png" alt="">
            </div>
            <div class="item">
              <img src="images/transitions1.png" alt="">
            </div>
            <div class="item">
              <img src="images/zeiss2.png" alt="">
            </div>
            <div class="item">
              <img src="images/cibavision1.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- footer -->
<?php include 'includes/footer.php'; ?>
