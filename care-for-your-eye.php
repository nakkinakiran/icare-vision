<!-- header -->

<?php

  $title = 'Care for your eye';

  include 'includes/header.php';

?>



<!-- body -->

<section>

  <div class="eye-test_cover">

    <div class="container">

      <div class="part14">

        <div class="row">

          <div class="col-lg-6 col-md-12 col-sm-12">

            <div class="content6 slideInLeft animated" data-animate="slideInLeft" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">

              <h2>Care for your eye</h2>

              <p>Your eyes are an important part of your health. Most people rely on their eyes to see and make sense of the world around them.</p>

            </div>

          </div>

          <div class="col-lg-6 col-md-12 col-sm-12 content_img">

            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">

              <img src="images/care-for-your-eye.png" alt="">

            </div>

          </div>

        </div>

        <div class="row">

          <div class="col-md-12 col-sm-12 eye-test_para2">

            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">

              <h2>Eye Care Tips</h2>

              <p>There are things you can do to help keep your eyes healthy and make sure you are seeing your best:</p>

              <ul>
                <li>
                  <p><b>Eat a healthy, balanced diet.</b> Your diet should include plenty or fruits and vegetables, especially deep yellow and green leafy vegetables. Eating fish high in omega-3 fatty acids, such as salmon, tuna, and halibut can also help your eyes.</p>
                </li>
                <li>
                  <p><b>Maintain a healthy weight.</b> Being overweight or having obesity increases your risk of developing diabetes. Having diabetes puts you at higher risk of getting diabetic retinopathy or glaucoma.</p>
                </li>
                <li>
                  <p><b>Get regular exercise.</b> Exercise may help to prevent or control diabetes, high blood pressure, and high cholesterol. These diseases can lead to some eye or vision problems. So if you exercise regularly, you can lower your risk of getting these eye and vision problems.</p>
                </li>
                <li>
                  <p><b>Wear sunglasses.</b> Sun exposure can damage your eyes and raise your risk of cataracts and age-related macular degeneration. Protect your eyes by using sunglasses that block out 99 to 100 percent of both UV-A and UV-B radiation.</p>
                </li>
                <li>
                  <p><b>Wear protective eye wear.</b> To prevent eye injuries, you need eye protection when playing certain sports, working in jobs such as factory work and construction, and doing repairs or projects in your home.</p>
                </li>
                <li>
                  <p><b>Avoid smoking.</b> Smoking increases the risk of developing age-related eye diseases such as macular degeneration and cataracts and can damage the optic nerve.</p>
                </li>
                <li>
                  <p><b>Know your family medical history.</b> Some eye diseases are inherited, so it is important to find out whether anyone in your family has had them. This can help you determine if you are at higher risk of developing an eye disease.</p>
                </li>
                <li>
                  <p><b>Know your other risk factors.</b> As you get older, you are at higher risk of developing age-related eye diseases and conditions. It is important to know you risk factors because you may be able to lower your risk by changing some behaviors.</p>
                </li>
                <li>
                  <p><b>If you wear contacts, take steps to prevent eye infections.</b> Wash your hands well before you put in or take out your contact lenses. Also follow the instructions on how to properly clean them, and replace them when needed.</p>
                </li>
                <li>
                  <p><b>Give your eyes a rest.</b> If you spend a lot of time using a computer, you can forget to blink your eyes and your eyes can get tired. To reduce eyestrain, try the 20-20-20 rule: Every 20 minutes, look away about 20 feet in front of you for 20 seconds.</p>
                </li>
              </ul>

              <h2 style="font-size: 30px;">Eye Tests and Exams</h2>

              <p>The exam includes several tests:</p>

            </div>

          </div>

        </div>

        <div class="row" style="padding-top:40px">

          <div class="col-lg-6 col-md-12 col-sm-12 content_img">

            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3s" style="animation-duration: 2s; visibility: visible;">

              <img src="images/eye-tests-exam.png" alt="">

            </div>

          </div>

          <div class="col-lg-6 col-md-12 col-sm-12">

            <div class="content7 bounceInRight animated" data-animate="bounceInRight" data-duration="2.5s" style="animation-duration: 1.5s; visibility: visible;">

              <table border="0" cellpadding="10" width="100%">

                <tr>

                  <td colspan="2">A visual field test to measure your side (peripheral) vision. A loss of peripheral vision may be a sign of glaucoma.</td>

                </tr>

                <tr>

                  <td colspan="2">A visual acuity test, where you read an eye chart about 20 feet away, to check on how well you see at various distances</td>


                </tr>

                <tr>


                  <td colspan="2">Tonometry, which measures your eye's interior pressure. It helps to detect glaucoma.</td>

                </tr>

                <tr>


                  <td colspan="2">Dilation, which involves getting eye drops that dilate (widen) your pupils. This allows more light to enter the eye. Your eye care provider examines your eyes using a special magnifying lens. This provides a clear view of important tissues at the back of your eye, including the retina, macula, and optic nerve.</td>

                </tr>

              </table>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<section>

  <div class="container">

    <div class="part12">

      <div class="row">

        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyeservice_modal">

          <h1 class="box_text">Eye Health Services</h1>

        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#children_modal">

          <img src="images/box2.jpg" alt="">

          <p>Children Eyetest</p>

        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecondition_modal">

          <img src="images/box3.jpg" alt="">

          <p>Eye Conditions</p>

        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecare_modal">

          <img src="images/box4.jpg" alt="">

          <p>Care for your eye</p>

        </div>

      </div>

    </div>

  </div>

</section>



<!-- horizontal line -->

<div class="horizontal_line1">

  <hr>

</div>



<section>

  <div class="container part2">

    <div class="row d-flex justify-content-center">

      <div class="col-lg-1"></div>

      <div class="col-lg-2 col-md-12 col-sm-12">

        <img src="images/nhs.png" alt="" width="125px" height="50px" class="nhs_icon">

      </div>

      <div class="col-lg-6 col-md-10 col-sm-10 test_content">

        <span>Do you qualify for a <b>Free Eye Test?</b></span></br>

        <p>Find out if you're entitled to a free NHS-funded Eye Test and optical vounchers.</p>

      </div>

      <div class="col-lg-1 col-md-2 col-sm-2">

        <a href="#" target="_self"><i class="fa fa-chevron-right"></i></a>

      </div>

    </div>

  </div>

</section>



<section>

  <div class="container">

    <div class="part15">

      <div class="content7 fadeIn animated" data-animate="fadeIn" data-duration="3s" style="animation-duration: 1.5s; visibility: visible;">

        <div class="book-eye-test_cover">

          <div class="row book-eye-test_content">

            <div class="col-lg-5 col-md-12 col-sm-12 ">

              <div class="content7 slideInLeft animated" data-animate="slideInLeft" data-duration="2s" style="animation-duration: 1.5s; visibility: visible;">

                <img src="images/eye-test_icon.png" alt="">

              </div>

            </div>

            <div class="col-lg-7 col-md-12 col-sm-12">

              <div class="content7 slideInRight animated" data-animate="slideInRight" data-duration="2s" style="animation-duration: 1.5s; visibility: visible;">

                <h3>Book an eye test today.</h3>

                <p><input type="text" name="" value="" placeholder="Postal Code"><a href="">Start Now</a></p>

                <p>or Call us for appointment <b style="margin-left: 10px;font-size:17px">024 7644 2272</b></p>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<section>

  <div class="container">

    <div class="row part7">

      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">

        <div class="contactlens_img">

          <img src="images/aboutus.png" alt="" class="contact_style fadeIn animated" data-animate="fadeIn" data-duration="2.5s">

          <div class="content7 fadeInUp animated" data-animate="fadeInUp" data-duration="1.5s" style="animation-duration: 1.5s; visibility: visible;">

            <h1>iCare Vision Specialists is an exclusive</br>optical showroom located in heart of</br>Conventry</h1></br>

            <a href="#">Know more<i class="fa fa-chevron-right" style="padding-left:28px;"></i></a>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<!-- horizontal line -->

<div class="horizontal_line1">

  <hr>

</div>



<section>

  <div class="container">

    <div class="row part8">

      <div class="col-lg-12 col-md-12 col-sm-12">

        <div class="text-center">

          <p>Brands we sell</p>

          <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel1">

            <div class="item">

              <img src="images/essilor1.png" alt="">

            </div>

            <div class="item">

            <img src="images/crizal2.png" alt="">

            </div>

            <div class="item">

              <img src="images/hoya1.png" alt="">

            </div>

            <div class="item">

              <img src="images/transitions1.png" alt="">

            </div>

            <div class="item">

              <img src="images/zeiss2.png" alt="">

            </div>

            <div class="item">

              <img src="images/cibavision1.png" alt="">

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<!-- footer -->

<?php include 'includes/footer.php'; ?>

