<!-- header -->
<?php
  $title = 'Contact Lenses';
  include 'includes/header.php';
?>

<!-- body -->
<section>
  <div class="contact_lenses_cover">
    <div class="container">
      <div class="part16">
        <div class="row">
          <div class="col-lg-8 col-md-12 col-sm-12">
            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <h2>Contact Lenses</h2>
              <p>iCare Vision offer a wide range of contact lenses</p><p> to suit individual preference.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="contactlense_block">
      <div class="row d-flex justify-content-center">
        <div class="col-lg-10 menu-block" style="padding-left:0px;padding-right:0px">
          <ul class="nav nav-tabs">
            <li class="">
              <a href="#disposables" data-toggle="tab" class="active">Daily<span>Disposables</span></a>
            </li>
            <li>
              <a href="#toric" data-toggle="tab">Daily <span>Toric</span></a>
            </li>
            <li>
              <a href="#mdisposables" data-toggle="tab">Monthly <span>Disposables</span></a>
            </li>
            <li>
              <a href="#mtoric" data-toggle="tab">Monthly <span>Toric</span></a>
            </li>
            <li>
              <a href="#wear" data-toggle="tab">Continous <span>Wear</span></a>
            </li>
            <li>
              <a href="#lenses" data-toggle="tab">Weekly <span>Lenses</span></a>
            </li>
            <li>
              <a href="#coloured" data-toggle="tab">Coloured <span>Lenses</span></a>
            </li>
          </ul>
        </div>
      </div>
      <div class="tab-content">
        <div class="row part17 tab-pane fadein active box_style" style="" id="disposables">
          <div class="col-lg-12 col-md-12 col-sm-12">
              <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel2">
                <div class="item">
                  <div class="lense_block">
                    <img src="images/biomedics.png" alt="">
                    <p>Biomedics 1 Day Extra(90 pairs)</p>
                    <!--<h6>&pound; 62.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/clariti.png" alt="">
                    <p>Clariti 1 Day Extra(90 pairs)</p>
                    <!--<h6>&pound; 62.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/clariti-multifocal(d).png" alt="">
                    <p>Clariti 1 Day Multifocal(90 pairs)</p>
                    <!--<h6>&pound; 62.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/dailies-aqua.png" alt="">
                    <p>Dailies AquaComfort Plus(90 pairs)</p>
                    <!--<h6>&pound; 62.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/dailiestotal.png" alt="">
                    <p>Dailies Total 1(90 Pairs)</p>
                    <!--<h6>&pound; 62.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/dailiesmultifocal.png" alt="">
                    <p>Focus AquaComfort Plus Multifocal(90 Pairs)</p>
                    <!--<h6>&pound; 62.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/acuvue.png" alt="">
                    <p>Johnson & Johnson 1-Day Acuvue Moist</p>
                    <!--<h6>&pound; 62.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/acuvuetrueye.png" alt="">
                    <p>Johnson & Johnson 1-Day Acuvue Tru-Eye</p>
                    <!--<h6>&pound; 62.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/acuvuemoist.png" alt="">
                    <p>Johnson & Johnson 1-Day Acuvue Moist Multifocal(90 Pairs)</p>
                    <!--<h6>&pound; 62.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="row part17 tab-pane fade box_style" style="" id="toric">
          <div class="col-lg-12 col-md-12 col-sm-12">
              <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel2">
                <div class="item">
                  <div class="lense_block">
                    <img src="images/alconfocus.png" alt="">
                    <p>Alocon Focus Dailies Toric (90 Pairs)</p>
                    <!--<h6>&pound; 112.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/coopervision-biomedics.png" alt="">
                    <p>CooperVision Biomedics 1 Day Toric(90 pairs)</p>
                    <!--<h6>&pound; 115.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/coopervision-clariti.png" alt="" style="">
                    <p>CooperVision Clariti 1 Day Toric (90 pairs)</p>
                    <!--<h6>&pound; 114.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/1day-acuvue-moist.png" alt="" style="margin-top: 7px;">
                    <p>Johnson & Johnson 1-Day Acuvue Moist For Astigmatism(90 pairs)</p>
                    <!--<h6>&pound; 114.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/acuvue-oasys.png" alt="" style="margin-top: 7px;">
                    <p>Johnson & Johnson Acuvue Oasys 1-Day for Astigmatism</p>
                    <!--<h6>&pound; 114.00</h6>-->
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="row part17 tab-pane fade box_style" style="" id="mdisposables">
          <div class="col-lg-12 col-md-12 col-sm-12">
              <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel2">
                <div class="item">
                  <div class="lense_block">
                    <img src="images/airoptix.png" alt="">
                    <p>Air Optix Aqua(6 Pairs) with Pure Moist and Travel Pack</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/avaira.png" alt="">
                    <p>CooperVision Avaira(6 Pairs)</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/biofinity.png" alt="" style="">
                    <p>CooperVision Biofinity(6 Pairs)</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/clariti-multifocal.png" alt="">
                    <p>CooperVision Clariti Elite(6 Pairs)</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/sauflon.png" alt="">
                    <p>CooperVision Clariti Multifocal(6 Pairs)</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="row part17 tab-pane fade box_style" style="" id="mtoric">
          <div class="col-lg-12 col-md-12 col-sm-12">
              <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel2">
                <div class="item">
                  <div class="lense_block">
                    <img src="images/airoptix-aqua.png" alt="">
                    <p>Air Optix Aqua for Astigmatism(6 Pairs)</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/airoptix-aqua.png" alt="">
                    <p>Air Optix Aqua for Astigmatism(6 Pairs) with Pure Moist Contact Lens Solutions</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/sauflon-clariti_toric.png" alt="" style="">
                    <p>Clariti Monthly Toric(6 Pairs)</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/biofinity.png" alt="" style="">
                    <p>CooperVision Biofinity Toric(6 Pairs)</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="row part17 tab-pane fade box_style" style="" id="wear">
          <div class="col-lg-12 col-md-12 col-sm-12">
              <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel2">
                <div class="item">
                  <div class="lense_block">
                    <img src="images/airoptix-night.png" alt="" style="">
                    <p>Air Optix Night & Day Aqua(6 Pairs)</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="row part17 tab-pane fade box_style" style="" id="lenses">
          <div class="col-lg-12 col-md-12 col-sm-12">
              <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel2">
                <div class="item">
                  <div class="lense_block">
                    <img src="images/acuvue-astig.png" alt="" style="">
                    <p>Johnson & Johnson Acuvue Oasys For Astigmatism</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
                <div class="item">
                  <div class="lense_block">
                    <img src="images/acuvue-plus.png" alt="" style="">
                    <p>Johnson & Johnson Acuvue Oasys Prebyopia(12 Pairs)</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div><div class="item">
                  <div class="lense_block">
                    <img src="images/acuvue-plus1.png" alt="" style="">
                    <p>Johnson & Johnson Acuvue Oasys with Hydraclear plus(12 Pairs)</p>
                    <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="row part17 tab-pane fade box_style" style="" id="coloured">
          <div class="col-lg-12 col-md-12 col-sm-12">
              <!--<div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel2">-->
                <p style="text-align:center">Coming Soon...</p>
              <!--</div>-->
          </div>
        </div>
      </div>
      <!-- <div class="row d-flex justify-content-center">
        <div class="col-lg-3 col-md-6 col-sm-12 lense_block">
          <img src="images/bioclear.png" alt="" style="margin-top: 7px;">
          <p>Bioclear Daily(90 pairs)</p>
          <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 lense_block">
          <img src="images/vision.png" alt="">
          <p>Bioclear Daily(90 pairs)</p>
          <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 lense_block">
          <img src="images/clariti.png" alt="">
          <p>Bioclear Daily(90 pairs)</p>
          <a href="#">Make an Enquiry<i class="fa fa-chevron-right" style="padding-left:25px;"></i></a>
        </div>
      </div> -->
    </div>
  </div>
</section>

<!-- horizontal line -->
<div class="horizontal_line1">
  <hr>
</div>

<section>
  <div class="container">
    <div class="row part8">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <p class="text-center">Brands we sell</p>
        <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel1">
          <div class="item">
            <img src="images/essilor1.png" alt="">
          </div>
          <div class="item">
          <img src="images/crizal2.png" alt="">
          </div>
          <div class="item">
            <img src="images/hoya1.png" alt="">
          </div>
          <div class="item">
            <img src="images/transitions1.png" alt="">
          </div>
          <div class="item">
            <img src="images/zeiss2.png" alt="">
          </div>
          <div class="item">
            <img src="images/cibavision1.png" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row part5">
      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
        <div class="frames_img">
          <img src="images/frames.png" alt="" class="frames_style">
          <div class="content5 fadeIn animated" data-animate="fadeIn" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
            <h2>Frames</h2>
            <p>we'll help you find the perfect Frames</p>
            </br>
            <a href="frames.php">Find out more<i class="fa fa-chevron-right" style="padding-left:20px;"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row part7">
      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
        <div class="contactlens_img">
          <img src="images/aboutus.png" alt="" class="contact_style fadeIn animated" data-animate="fadeIn" data-duration="2.5s">
          <div class="content7 fadeInUp animated" data-animate="fadeInUp" data-duration="1.5s" style="animation-duration: 1.5s; visibility: visible;">
            <h1>iCare Vision Specialists is an exclusive</br>optical showroom located in heart of</br>Conventry</h1>
            </br>
            <a href="#">Know more<i class="fa fa-chevron-right" style="padding-left:28px;"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- footer -->
<?php include 'includes/footer.php'; ?>
