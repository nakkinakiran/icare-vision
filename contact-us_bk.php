<!-- header -->
<?php
$title = 'Contact Us';
include 'includes/header.php'; ?>

<!-- body -->
<!-- part1 -->
<section>
  <div class="contact_part1">
    <img src="images/contact_cover.jpg" alt="">
    <p>Book an Eyetest<a href="#" target="_self">Click here <i class="fa fa-chevron-right"></i></a></p>    
  </div>
</section>
<!-- part2 -->
<section>
  <div class="container">
    <div class="row contact_part2">
      <div class="col-md-6 col-sm-12" style="padding:10px 30px 10px 20px;border-right:1px solid #015dba">
        <div class="pulse animated" data-animate="pulse" data-duration="3.0s" style="animation-duration: 3s; visibility: visible;">
          <h2 style="font-family:Roboto 'Bold';font-weight:bold;font-size:30px;padding:35px 0px 75px 0px;text-align:center">Contact <span>Lenses</span></h2>
          <table border="0" width="100%" style="font-size:13px;font-weight:500;" cellpadding="5">
              <tr>
              </tr>
              <tr>
              <td rowspan="2" style="font-size:16px;color: #015dba;text-align:center"><i class="fas fa-clock"></i>Opening Hours</td>
              <td>Mon - Sat</td>
              <td>9am - 5:30pm</td>
            </tr>
            <tr>
              <td>Closed for lunch</td>
              <td>1pm - 2pm</td>
            </tr>
            <tr>
              <td></td>
            </tr>
            <tr>
              <td style="font-size:32px;text-align:center"><i class="fa fa-map-marker" style="color:#015dba;"></i></td>
              <td>	291 - 293 Walsgrave Rd </br>Coventry CV2 4BE</td>
            </tr>
            <tr>
              <td></td>
            </tr>
            <tr>
              <td style="font-size:26px;text-align:center"><i class="fa fa-phone" style="color:#015dba;"></i></td>
              <td>02476 442 272</td>
            </tr>
          </table>
        </div>
      </div>
      <div class="col-md-6 col-sm-12" style="padding:40px 30px 10px 20px;">
        <div class="contact_content1 pulse animated" data-animate="pulse" data-duration="3.0s" style="animation-duration: 3s; visibility: visible;">
          <h4 class="text-center mb-3" style="color:#015dba;">Our staff are always ready to help and</br> we will respond to your query</br> as soon as possible.</h4>
          <form action="#" method="POST">
            <div class="row form_style">
              <div class="col-md-6 col-sm-12">
                <input type="text" name="" value="" class="form-control" placeholder="Full Name">
              </div>
              <div class="col-md-6 col-sm-12">
                <input type="tel" name="" value="" class="form-control" placeholder="Contact Number">
              </div>
            </div>
            <div class="row form_style">
              <div class="col-md-12 col-sm-12">
                <input type="email" name="" value="" class="form-control" placeholder="Email Address">
              </div>
              <div class="col-md-6 col-sm-12">
              </div>
            </div>
            <div class="row form_style">
              <div class="col-md-12 col-sm-12">
                <textarea name="name" rows="4" cols="0" class="form-control" placeholder="Write your query here..."></textarea>
              </div>
            </div>
            <div class="row form_style">
              <div class="col-md-12 col-sm-12">
                <a href="#">Submit<i class="fa fa-arrow-right" style="padding-left:30px;"></i></a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <hr>
    <div class="row contact_part2">
      <div class="col-md-12 col-sm-12">
        <!-- <span class="" style="font-size:24px;text-align:center"><i class="fa fa-map-marker"></i></span> -->
        <section>
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d27534.478564292716!2d-1.4807939834777795!3d52.41162266285492!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48774bdec6c462a9%3A0xdd66c3a5ac2fcf89!2s291+Walsgrave+Rd%2C+Coventry+CV2+4AX%2C+UK!5e0!3m2!1sen!2sus!4v1550833541755" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
          <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1240.5643569908477!2d0.0352678240249117!3d51.547538760719505!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a7aff29f993b%3A0xf886885b93498087!2sStudy.!5e0!3m2!1sen!2sin!4v1546876496012" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe> -->
        </section>
      </div>
    </div>
  </div>
</section>

<!-- footer -->
<?php include 'includes/footer.php'; ?>
