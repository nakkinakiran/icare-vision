<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <!-- company logo -->
    <link rel="icon" type="image/png" href="images/logo.png">
    <!-- custom styles -->
    <link rel="stylesheet" href="css/style.css">
    <!-- media query styles -->
    <link rel="stylesheet" href="css/media.css">
    <!-- Roboto google font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <!-- bootstrap style -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- font-awesome -->
    <link rel="stylesheet" href="node_modules/css/font-awesome.css">
    <!-- owl-carousel -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- owl-carousel theme -->
  	<link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!-- animations -->
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="clockpicker/dist/bootstrap-clockpicker.min.css">
    <!-- calendar -->
    <link rel="stylesheet" href="pg-calendar/dist/css/pignose.calendar.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- jquery -->
    <script src="js/jquery.min.js"></script>
    <!-- bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <script src="clockpicker/dist/bootstrap-clockpicker.min.js"></script>
    <!-- scrolla animation -->
    <script src="js/scrolla.jquery.min.js"></script>
    <!-- calendar script -->
    <script src="pg-calendar/dist/js/pignose.calendar.min.js"></script>
    <script src="pg-calendar/dist/js/pignose.calendar.full.min.js"></script>
    <style media="screen">
      .sidemenu{
        height: 100%;
        width: 0;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background: linear-gradient(to left, #FCE48A 0%, #88DAF0 100%);
        opacity: 1;
        overflow-x: hidden;
        transition: .5s;
        padding-top: 60px;
      }
      .sidemenu a{
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 25px;
        color: #000;
        display: block;
        transition: .3s;
      }
      .sidemenu a:hover {
        color: #015dba;
      }
      .sidemenu .closebtn {
        position: absolute;
        top: 0;
        right: 25px;
        font-size: 36px;
        margin-left: 50px;
      }
      nav ul a{
        font-family: Roboto 'Regular';
        font-size: 15px;
        text-decoration: none;
        color: #000;
        font-weight: 500;
        margin: 0px 0px 0px 5px;
        padding: 0px 5px 21px 5px;
      }
      nav ul a:hover{
        font-family: Roboto 'Regular';
        font-size: 15px;
        color: #015dba;
        font-weight: 500;
        border-bottom : 3px solid #015dba;
      }
      .brand-logo{
        display: none;
      }
      @media screen and (max-height: 450px) {
        .sidemenu { padding-top: 15px; }
        .sidemenu a{ font-size: 18px; }
      }
      @media (min-width: 320px) and (max-width: 991px) {
        .brand-logo{
          display: block;
        }
        .sidemenu .appointment_link{
          width: 64%;
          font-size: 16px!important;
        }
        .sidemenu .appointment_link:hover{
          width: 66%;
        }
        .sidemenu a{
          font-size: 20px!important;
        }
        .sidemenu ul li{
          display: inline-block;
        }
        .sidemenu ul li a{
          font-size: 32px!important;
          padding-left: 0px!important;
        }
      }
      @media (min-width: 768px) and (max-width: 991px) {
        .sidemenu .appointment_link{
          width: 25%;
        }
        .sidemenu .appointment_link:hover{
          width: 27%;
        }
      }
    </style>
  </head>
  <body>
    <header id="header">
      <?php $pagename = basename($_SERVER['PHP_SELF']);
      if($pagename == 'index.php'){
        $aclass = 'class="active"';
      }
      else if($pagename == 'what-we-do.php'){
        $bclass = 'class="active"';
      }
      else if($pagename == 'contact-us.php'){
        $cclass = 'class="active"';
      }
      else if($pagename == 'frames.php'){
        $dclass = 'class="active"';
      }
      else if($pagename == 'contact-lenses.php'){
        $eclass = 'class="active"';
      }
      else if($pagename == 'eye-health.php'){
        $fclass = 'class="active"';
      }
      else if($pagename == 'eye-test.php'){
        $aclass = 'class="active"';
      }
      else if($pagename == 'book-appointment.php'){
        $aclass = 'class="active"';
      }
      ?>
      <div class="container">
        <div class="row header_cont circleBehind navbar navbar-expand-lg navbar-expand-md">
          <a class="brand-logo" href="index.php"><img src="images/logo3.png" width="128px"></a>
          <button class="navbar-toggler ml-auto" type="button" data-target="#mysidemenu" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span style="font-size: 30px;" onclick="openNav()">&#9776;</span>
          </button>
          <div class="collapse navbar-collapse">
            <div class="col-md-5 col-sm-12">
              <div class="social_icons">
                <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook-square"></i></a>
                <a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter-square"></i></a>
                <a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
              </div>
              <hr>
              <div class="nav-links nav_links_left">
                <a href="index.php" <?php echo @$aclass; ?> target="_self">Home</a>
                <a href="what-we-do.php"  <?php echo @$bclass; ?> target="_self">What we do</a>
                <a href="contact-us.php" <?php echo @$cclass; ?> target="_self">Contact Us</a>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <a href="index.php">
                <img src="images/logo.png" alt="">
              </a>
            </div>
            <div class="col-md-5 col-sm-12 right_nav">
              <!-- <a href="book-appointment.php"  class="appointment_link " target="_self"> -->
              <a class="appointment_link" onclick="openEyetest()" data-target="#myslide" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span>Book an appointment<i class="fa fa-chevron-right"></i></span>
              </a>
              <!-- </a> -->
              <hr>
              <div class="nav-links nav_links_right">
                <a href="frames.php" <?php echo @$dclass; ?> target="_self">Frames</a>
                <a href="contact-lenses.php" <?php echo @$eclass; ?> target="_self">Contact Lenses</a>
                <a href="eye-health.php" <?php echo @$fclass; ?> target="_self">Eye Health</a>
              </div>
            </div>
            <!-- slide data content -->
            <div id="myslide" class="slide">
              <a href="javascript:void(0)" class="closebtn" onclick="closeEyetest()">&times;</a>
              <div class="container">
                <form class="book_app" action="#" method="post">
                  <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12">
                      <img src="images/contact.png" alt="" width="175px">
                      <h1>Book your Eye Test</h1>
                      <p>Enter your details and select a store and time to book now. See you soon!</p>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                      <h5>Personal information</h5>
                      <div class="">
                        <input type="text" name="" value="" placeholder="Full Name" class="form-control">
                      </div>
                      <div class="">
                        <input type="text" name="" value="" placeholder="Email Address" class="form-control">
                      </div>
                      <div class="">
                        <input type="text" name="" value="" placeholder="Phone Number" class="form-control">
                      </div>
                      <div class="">
                        <input type="text" name="" value="" placeholder="Zip Code" class="form-control">
                      </div>
                      <div class="">
                        <textarea name="name" rows="3" cols="50" placeholder="Address" class="form-control"></textarea>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                      <h5>Appointment Details</h5>
                      <ul>
                         <li>
                            <h6>Appoinment type</h6>
                         </li>
                          <li>
                            <input type="checkbox" id="check1">
                            <label for="check1">Eye Examination</label>
                          </li>
                          <li>
                            <input type="checkbox" id="check2">
                            <label for="check2">FREE Contact Lens Comfort Trial</label>
                          </li>
                          <li>
                            <input type="checkbox" id="check3">
                            <label for="check3">Contact Lens Fitting/Care Appointment</label>
                          </li>
                          <li>
                            <input type="checkbox" id="check4">
                            <label for="check4">Contact Lens Fitting/Care Appointment</label>
                            <span id="check1_error"></span>
                          </li>
                      </ul>
                      <table cellpadding="8" align="center" width="100%">
                        <!--<tr>-->
                        <!--  <th>Appoinment type</th>-->
                        <!--</tr>-->
                        <!--<tr>-->
                        <!--  <td>-->
                            <!-- <input type="checkbox" name="" value="" id="check1" onblur="validation();"> Eye Examination</br> -->
                        <!--    <input type="checkbox" id="check1">-->
                        <!--    <label for="check1">Eye Examination</label>-->
                        <!--  </td>-->
                        <!--</tr>-->
                        <!--<tr>-->
                        <!--  <td colspan="2">-->
                            <!-- <input type="checkbox" name="" value="" id="check2" onblur="validation();"> FREE Contact Lens Comfort Trial -->
                        <!--    <input type="checkbox" id="check2">-->
                        <!--    <label for="check2">FREE Contact Lens Comfort Trial</label>-->
                        <!--  </td>-->
                        <!--</tr>-->
                        <!--<tr>-->
                        <!--  <td colspan="2">-->
                            <!-- <input type="checkbox" name="" value="" id="check3" onblur="validation();"> Contact Lens Fitting/Care Appointment -->
                        <!--    <input type="checkbox" id="check3">-->
                        <!--    <label for="check3">Contact Lens Fitting/Care Appointment</label>-->
                        <!--  </td>-->
                        <!--</tr>-->
                        <!--<tr>-->
                        <!--  <td colspan="2">-->
                            <!-- <input type="checkbox" name="" value="" id="check4" onblur="validation();"> Co-operate Eye Care Voucher</br> -->
                        <!--    <input type="checkbox" id="check4">-->
                        <!--    <label for="check4">Contact Lens Fitting/Care Appointment</label>-->
                        <!--    <span id="check1_error"></span></td>-->
                        <!--</tr>-->
                        <tr>
                          <th>Appointment 1<sup>st</sup> Choice</th>
                        </tr>
                        <tr>
                          <td><input type="date" name="" value="" id="text-calendar" class="form-control calendar fa fa-calendar"></td>
                          <td><input type="text" id="single-input" class="form-control" name="time" required="" placeholder="Time">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <span class="ti-time"></span></td>
                        </tr>
                        <tr>
                          <th>Appointment 2<sup>nd</sup> Choice</th>
                        </tr>

                        <tr>
                          <td><input type="date" name="" value="" id="text-calendar" class="form-control calendar fa fa-calendar"></td>
                          <td><input type="text" id="single-input1" class="form-control" name="time" required="" placeholder="Time">
                            <span class="highlight"></span>
                              <span class="bar"></span>
                            <span class="ti-time"></span></td>
                        </tr>

                        <tr>
                          <td colspan="2">
                            <textarea class="form-control" name="message" placeholder="Message"></textarea>
                            <span class="highlight"></span>
                              <span class="bar"></span>
                            <span class="ti-time"></span></td>
                        </tr>

                        <tr>
                          <td colspan="2" style="padding:20px 0px 80px 10px;">
                            <a href="#">CONFIRM YOUR REQUEST<i class="fa fa-chevron-right"></i></a>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div id="mysidemenu" class="sidemenu">
          <img src="images/logo3.png" width="128px" alt="" style="display:block;margin-left: auto;margin-right:auto;">
          <a href="book-appointment.php" class="appointment_link" target="_self">Book an appointment<i class="fa fa-chevron-right" style="padding-left:5px;"></i></a>
          <a href="javascript:void(0)" class="closebtn" onclick="closeNav()" style="font-size: 32px!important;">&times;</a>
          <a href="index.php">Home</a>
          <a href="what-we-do.php">What we do</a>
          <a href="contact-us.php">Contactus</a>
          <a href="frames.php">Frames</a>
          <a href="contact-lenses.php">Contact Lenses</a>
          <a href="eye-health.php">Eye Health</a>
          <ul style="text-align:center!important;padding-left:0px!important">
            <li><a href="https://www.facebook.com/" target="_blank" style="color:#3b5998"><i class="fa fa-facebook-square"></i></a></li>
            <li><a href="https://twitter.com/" target="_blank" style="color: #1DA1F2;"><i class="fa fa-twitter-square"></i></a></li>
            <li><a href="https://www.instagram.com/" target="_blank" style="color: #C13584;"><i class="fa fa-instagram"></i></a>  </li>
          </ul>
        </div>
      </div>
    </header>
  </body>
</html>
<!-- sidemenu -->
<script type="text/javascript">
  function openNav(){
    document.getElementById('mysidemenu').style.width = "100%";
  }
  function closeNav(){
    document.getElementById('mysidemenu').style.width = "0px";
  }
</script>

<!-- book-eyetest slide -->
<script type="text/javascript">
  function openEyetest(){
    document.getElementById('myslide').style.width = "100%";
  }
  function closeEyetest(){
    document.getElementById('myslide').style.width = "0px";
  }
</script>
