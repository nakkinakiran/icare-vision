<!-- header -->
<?php
  $title = 'Eye-test';
  include 'includes/header.php';
?>

<!-- body -->
<section>
  <div class="eye-test_cover">
    <div class="container">
      <div class="part14">
        <div class="row">
          <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="content6 slideInLeft animated" data-animate="slideInLeft" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <h2>Eye Test</h2>
              <p>We take vision seriously. Which is why an Eye Test is the most important service we provide.</p>
            </div>
          </div>
          <div class="col-lg-6 col-md-12 col-sm-12 content_img">
            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <img src="images/eye-test_bnr1.png" alt="">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 eye-test_para2">
            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <p>To maintain good eye health and vision, it is important to have your eyes tested every two years or more frequently if advised. You should seek advice immediately if you have any sudden loss of vision, eye pain, inflammation or irritation.</p>
              <p>Most eye diseases can be found during a routine eye test and treated when detected at an early stage. If you have health problems, such as diabetes, you may need to visit your optometrist more frequently to detect any complicatons.</p>
              <h2 style="font-size: 30px;">The Technology</h2>
              <p>Although the names may be unfamiliar, these are the pieces of equipment used most often in an Eye Test. If you have any questions about our testing equipment, your Optometrist will be happy to explain.</p>
            </div>
          </div>
        </div>
        <div class="row" style="padding-top:40px">
          <div class="col-lg-6 col-md-12 col-sm-12 content_img">
            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3s" style="animation-duration: 2s; visibility: visible;">
              <img src="images/eye-test_bnr2.png" alt="">
            </div>
          </div>
          <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="content7 bounceInRight animated" data-animate="bounceInRight" data-duration="2.5s" style="animation-duration: 1.5s; visibility: visible;">
              <table border="0" cellpadding="10" width="100%">
                <tr>
                  <td>Applanation method</td>
                  <td>Slit-lamp exam</td>
                </tr>
                <tr>
                  <td>Corneal and retinal topography</td>
                  <td>Tonometry</td>
                </tr>
                <tr>
                  <td>Fluorescein angiogram</td>
                  <td>Ultrasound</td>
                </tr>
                <tr>
                  <td>Pupillary dilation test</td>
                  <td>Visual acuity testing</td>
                </tr>
                <tr>
                  <td>Refraction test</td>
                  <td>visual field test</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="part12">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyeservice_modal">
          <h1 class="box_text">Eye Health Services</h1>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#children_modal">
          <img src="images/box2.jpg" alt="">
          <p>Children Eyetest</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecondition_modal">
          <img src="images/box3.jpg" alt="">
          <p>Eye Conditions</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecare_modal">
          <img src="images/box4.jpg" alt="">
          <p>Care for your eye</p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- horizontal line -->
<div class="horizontal_line1">
  <hr>
</div>

<section>
  <div class="container part2">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-1"></div>
      <div class="col-lg-2 col-md-12 col-sm-12">
        <img src="images/nhs.png" alt="" width="125px" height="50px" class="nhs_icon">
      </div>
      <div class="col-lg-6 col-md-10 col-sm-10 test_content">
        <span>Do you qualify for a <b>Free Eye Test?</b></span></br>
        <p>Find out if you're entitled to a free NHS-funded Eye Test and optical vounchers.</p>
      </div>
      <div class="col-lg-1 col-md-2 col-sm-2">
        <a href="#" target="_self"><i class="fa fa-chevron-right"></i></a>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="part15">
      <div class="content7 fadeIn animated" data-animate="fadeIn" data-duration="3s" style="animation-duration: 1.5s; visibility: visible;">
        <div class="book-eye-test_cover">
          <div class="row book-eye-test_content">
            <div class="col-lg-5 col-md-12 col-sm-12 ">
              <div class="content7 slideInLeft animated" data-animate="slideInLeft" data-duration="2s" style="animation-duration: 1.5s; visibility: visible;">
                <img src="images/eye-test_icon.png" alt="">
              </div>
            </div>
            <div class="col-lg-7 col-md-12 col-sm-12">
              <div class="content7 slideInRight animated" data-animate="slideInRight" data-duration="2s" style="animation-duration: 1.5s; visibility: visible;">
                <h3>Book an eye test today.</h3>
                <p><input type="text" name="" value="" placeholder="Postal Code"><a href="">Start Now</a></p>
                <p>or Call us for appointment <b style="margin-left: 10px;font-size:17px">024 7644 2272</b></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row part7">
      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
        <div class="contactlens_img">
          <img src="images/aboutus.png" alt="" class="contact_style fadeIn animated" data-animate="fadeIn" data-duration="2.5s">
          <div class="content7 fadeInUp animated" data-animate="fadeInUp" data-duration="1.5s" style="animation-duration: 1.5s; visibility: visible;">
            <h1>iCare Vision Specialists is an exclusive</br>optical showroom located in heart of</br>Conventry</h1>
            <a href="#">Know more<i class="fa fa-chevron-right" style="padding-left:28px;"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- horizontal line -->
<div class="horizontal_line1">
  <hr>
</div>

<section>
  <div class="container">
    <div class="row part8">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="text-center">
          <p>Brands we sell</p>
          <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel1">
            <div class="item">
              <img src="images/essilor1.png" alt="">
            </div>
            <div class="item">
              <img src="images/crizal2.png" alt="">
            </div>
            <div class="item">
              <img src="images/hoya1.png" alt="">
            </div>
            <div class="item">
              <img src="images/transitions1.png" alt="">
            </div>
            <div class="item">
              <img src="images/zeiss2.png" alt="">
            </div>
            <div class="item">
              <img src="images/cibavision1.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- footer -->
<?php include 'includes/footer.php'; ?>
