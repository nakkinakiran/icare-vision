<!-- header -->
<?php
  $title = 'Frames';
  include 'includes/header.php';
?>

<!-- body -->
<section>
  <div class="eye-health_cover">
    <div class="container">
      <div class="part13">
        <div class="row">
          <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="content6 slideInLeft animated" data-animate="slideInLeft" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <h2>Frames</h2>
              <p>Choosing the right pair of glasses isn't just about the frame and how it looks on your face. </p>
            </div>
          </div>
          <div class="col-lg-6 col-md-12 col-sm-12 content_img">
            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <img src="images/eye-health_bnr.png" alt="">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 eye-health_para2">
            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">
              <p>Selection starts with your prescription, which guides your choice of lenses and the frame styles best suited to your lens types. Ultimately, your prescription will help you narrow down the options for you from our great selection of glasses available. Then of course, it's about finding the right pair of glasses to match your style and your budget.</p>
              <p>Most prescriptions are compatible with all frames, but complex or high index prescriptions can sometimes limit your style choice. This is why our local opticians provide you with expert free advice to match your prescription, face shape and frame. This means you'll leave with the perfect pair of glasses for you. You can view a wide range of glasses online - then we always recommend coming to see your local optician, where we can spend time finding your ideal pair. Why not book an eye test today?</p>
              <p>Our extensive range of Spectacle Frames, reflect the wide variety of styles and materials currently available. Advice on the most suitable frame style and lens type for your prescription will be given by our staff to enable you choose the perfect pair of Spectacles,Sunglasses and Prescription Sunglasses.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="part12">
      <p>Eye Health Services</p>
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyetest_modal">
          <img src="images/box1.jpg" alt="">
          <p>Eyetest</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#children_modal">
          <img src="images/box2.jpg" alt="">
          <p>Children Eyetest</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecondition_modal">
          <img src="images/box3.jpg" alt="">
          <p>Eye Conditions</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecare_modal">
          <img src="images/box4.jpg" alt="">
          <p>Care for your eye</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row part6">
      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
        <div class="contactlens_img">
          <img src="images/contactlens.png" alt="" class="contact_style">
          <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
            <h2>Contact Lenses</h2>
            <p>we'll help you find the perfect Frames</p>
            <a href="#">Find out more<i class="fa fa-chevron-right" style="padding-left:20px;"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row part7">
      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
        <div class="contactlens_img">
          <img src="images/aboutus.png" alt="" class="contact_style fadeIn animated" data-animate="fadeIn" data-duration="2.5s">
          <div class="content7 fadeInUp animated" data-animate="fadeInUp" data-duration="1.5s" style="animation-duration: 1.5s; visibility: visible;">
            <h1>iCare Vision Specialists is an exclusive</br>optical showroom located in heart of</br>Conventry</h1>
            <a href="#">Know more<i class="fa fa-chevron-right" style="padding-left:28px;"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- horizontal line -->
<div class="horizontal_line1">
  <hr>
</div>

<section>
  <div class="container">
    <div class="row part8">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="text-center">
          <p>Brands we sell</p>
          <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel1">
            <div class="item">
              <img src="images/essilor1.png" alt="">
            </div>
            <div class="item">
            <img src="images/crizal2.png" alt="">
            </div>
            <div class="item">
              <img src="images/hoya1.png" alt="">
            </div>
            <div class="item">
              <img src="images/transitions1.png" alt="">
            </div>
            <div class="item">
              <img src="images/zeiss2.png" alt="">
            </div>
            <div class="item">
              <img src="images/cibavision1.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<!-- footer -->
<?php include 'includes/footer.php'; ?>
