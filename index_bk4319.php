<?php
  $title = 'iCare Vision';
  include 'includes/header.php';
?>

<!-- part1 -->
<section>
  <div class="part1">
    <div class="col-md-12 col-sm-12 image">
      <img src="images/banner.png" alt="">
      <a href="#">Shop Now<i class="fa fa-chevron-right" style="padding-left:20px;"></i></a>
    </div>
  </div>
</section>

<!-- part2 -->
<section>
  <div class="container part2">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-1"></div>
      <div class="col-lg-2">
        <img src="images/nhs.png" alt="" width="125px" height="50px" class="nhs_icon">
      </div>
      <div class="col-lg-6 test_content">
        <span>Do you qualify for a <b>Free Eye Test?</b></span></br>
        <p>Find out if you're entitled to a free NHS-funded Eye Test and optical vounchers.</p>
      </div>
      <div class="col-lg-1">
        <a href="#" target="_self"><i class="fa fa-chevron-right"></i></a>
      </div>
    </div>
  </div>
</section>

<!-- part3 -->
<section>
  <div class="container">
    <div class="row d-flex justify-content-center part3">
      <div class="col-lg-6 col-md-12 col-sm-12 p1">
        <img src="images/eyetest.png" alt="" class="img_property">
        <div class="content1 slideInLeft animated" data-animate="slideInLeft" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
          <h2>Eyetest</h2>
          <p>Our optometrists have over 100</br>ways of assessing your vision</br>and eye health</p>
          <a href="#">Find out more<i class="fa fa-chevron-right" style="padding-left:20px;"></i></a>
        </div>
      </div>
      <div class="col-lg-6 col-md-12 col-sm-12 p2">
        <img src="images/childrentest.png" alt="" class="img_property1">
        <div class="content2 slideInRight animated" data-animate="slideInRight" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
          <h2>Children Eyetest</h2>
          <p>It's important to get kids tested</br>early, so that any potential prob-</br>lens can be identified.</p>
          <a href="#">Find out more<i class="fa fa-chevron-right" style="padding-left:20px;"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- part4 -->
<section>
  <div class="container">
    <div class="row d-flex justify-content-center part4">
      <div class="col-lg-6 col-md-6 col-sm-12 p3">
        <img src="images/eye.png" alt="" class="img_property2">
        <div class="content3 slideInLeft animated" data-animate="slideInLeft" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
          <h2>Eye Conditions</h2>
          <p>The eyes are complex organs. There</br>are many parts that must work</br>together to produce clear vision.</p>
          <a href="#">Find out more<i class="fa fa-chevron-right" style="padding-left:20px;"></i></a>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 p4">
        <img src="images/eyeconditions.png" alt="" class="img_property3">
        <div class="content4 slideInRight animated" data-animate="slideInRight" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
          <h2>Care for your eye</h2>
          <p>It's important to get kids tested</br>early, so that any potential prob-</br>lens can be identified.</p>
          <a href="#">Find out more<i class="fa fa-chevron-right" style="padding-left:20px;"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- part5 -->
<section>
  <div class="container">
    <div class="row part5">
      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
        <div class="frames_img">
          <img src="images/frames.png" alt="" class="frames_style">
          <div class="content5 fadeIn animated" data-animate="fadeIn" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
            <h2>Frames</h2>
            <p>we'll help you find the perfect Frames</p>
            <a href="#">Find out more<i class="fa fa-chevron-right" style="padding-left:20px;"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- part6 -->
<section>
  <div class="container">
    <div class="row part6">
      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
        <div class="contactlens_img">
          <img src="images/contactlens.png" alt="" class="contact_style">
          <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="2.0s" style="animation-duration: 2s; visibility: visible;">
            <h2>Contact Lenses</h2>
            <p>we'll help you find the perfect Frames</p>
            <a href="#">Find out more<i class="fa fa-chevron-right" style="padding-left:20px;"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- part7 -->
<section>
  <div class="container">
    <div class="row part7">
      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">
        <div class="contactlens_img">
          <img src="images/aboutus.png" alt="" class="contact_style fadeIn animated" data-animate="fadeIn" data-duration="2.5s">
          <div class="content7 fadeInUp animated" data-animate="fadeInUp" data-duration="1.5s" style="animation-duration: 1.5s; visibility: visible;">
            <h1>iCare Vision Specialists is an exclusive</br>optical showroom located in heart of</br>Conventry</h1>
            <a href="#">Know more<i class="fa fa-chevron-right" style="padding-left:28px;"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- horizontal line -->
<div class="horizontal_line1">
  <hr>
</div>

<!-- part8 -->
<section>
  <div class="container">
    <div class="row part8">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="text-center">
          <p>Brands we sell</p>
          <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel1">
            <div class="item">
              <div class="row brands">
                <div class="col-lg-2 col-md-4 col-sm-6">
                  <img src="images/essilor.png" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                  <img src="images/crizal.png" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                  <img src="images/hoya.png" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                  <img src="images/transitions.png" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                  <img src="images/zeiss.png" alt="" width="25px" height="150px">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                  <img src="images/cibavision.png" alt="">
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row brands">
                <div class="col-lg-2 col-md-4 col-sm-6">
                  <img src="images/essilor.png" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                  <img src="images/crizal.png" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                  <img src="images/hoya.png" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                  <img src="images/transitions.png" alt="">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                  <img src="images/zeiss.png" alt="" width="25px" height="150px">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                  <img src="images/cibavision.png" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include 'includes/footer.php';?>
