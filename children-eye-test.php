<!-- header -->

<?php

  $title = 'Children eye test';

  include 'includes/header.php';

?>



<!-- body -->

<section>

  <div class="eye-test_cover">

    <div class="container">

      <div class="part14">

        <div class="row">

          <div class="col-lg-6 col-md-12 col-sm-12">

            <div class="content6 slideInLeft animated" data-animate="slideInLeft" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">

              <h2>children Eye Test</h2>

              <p>All children under 16 qualify for an NHS voucher entitling them to a free eye test.</p>

            </div>

          </div>

          <div class="col-lg-6 col-md-12 col-sm-12 content_img">

            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">

              <img src="images/children-eye-test.png" alt="">

            </div>

          </div>

        </div>

        <div class="row">

          <div class="col-md-12 col-sm-12 eye-test_para2">

            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3.0s" style="animation-duration: 2s; visibility: visible;">

              <p>We know an Eye Test can be a little overwhelming for some children. Especially if it’s their first one. That’s why we’ve made the whole process as welcoming, as fun and as child friendly as possible.</p>

              <p>While there are aspects which are similar to an adult Eye Test, the children’s test does use equipment and a number of techniques which have been adapted to their specific needs. For instance, if a child is unable to read, the familiar letters chart is replaced with one using pictures and shapes.</p>

              <p>This doesn’t mean your child’s Eye Test will be any less thorough or insightful - allowing your Optometrist to make an in-depth assessment of eye health, vision and expected levels of development and growth.</p>

              <h2 style="font-size: 30px;">Spotting the signs</h2>

              <p>A report by the Association of Optometrists has highlighted the fact that one in five children have an undetected eye problem.</p>

            </div>

          </div>

        </div>

        <div class="row" style="padding-top:40px">

          <div class="col-lg-6 col-md-12 col-sm-12 content_img">

            <div class="content6 fadeIn animated" data-animate="fadeIn" data-duration="3s" style="animation-duration: 2s; visibility: visible;">

              <img src="images/spotting-signs.png" alt="">

            </div>

          </div>

          <div class="col-lg-6 col-md-12 col-sm-12">

            <div class="content7 bounceInRight animated" data-animate="bounceInRight" data-duration="2.5s" style="animation-duration: 1.5s; visibility: visible;">

              <table border="0" cellpadding="10" width="100%">

                <tr>

                  <td>Frequent squinting or frowning</td>

                  <td>Excessive eye rubbing</td>

                </tr>

                <tr>

                  <td>Closing one eye when watching TV or Reading</td>

                  <td>Tired eyes or headches</td>

                </tr>

                <tr>

                  <td>Skipping words or whole lines when reading</td>

                  <td>Difficulty seeing text from a board</td>

                </tr>

                <tr>

                  <td>Poor literacy</td>

                  <td>Sensivity to light</td>

                </tr>

                

              </table>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<section>

  <div class="container">

    <div class="part12">

      <div class="row">

        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyeservice_modal">

          <h1 class="box_text">Eye Health Services</h1>

        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#children_modal">

          <img src="images/box2.jpg" alt="">

          <p>Children Eyetest</p>

        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecondition_modal">

          <img src="images/box3.jpg" alt="">

          <p>Eye Conditions</p>

        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 eyehealth_services" data-toggle="modal" data-target="#eyecare_modal">

          <img src="images/box4.jpg" alt="">

          <p>Care for your eye</p>

        </div>

      </div>

    </div>

  </div>

</section>



<!-- horizontal line -->

<div class="horizontal_line1">

  <hr>

</div>



<section>

  <div class="container part2">

    <div class="row d-flex justify-content-center">

      <div class="col-lg-1"></div>

      <div class="col-lg-2 col-md-12 col-sm-12">

        <img src="images/nhs.png" alt="" width="125px" height="50px" class="nhs_icon">

      </div>

      <div class="col-lg-6 col-md-10 col-sm-10 test_content">

        <span>Do you qualify for a <b>Free Eye Test?</b></span></br>

        <p>Find out if you're entitled to a free NHS-funded Eye Test and optical vounchers.</p>

      </div>

      <div class="col-lg-1 col-md-2 col-sm-2">

        <a href="#" target="_self"><i class="fa fa-chevron-right"></i></a>

      </div>

    </div>

  </div>

</section>



<section>

  <div class="container">

    <div class="part15">

      <div class="content7 fadeIn animated" data-animate="fadeIn" data-duration="3s" style="animation-duration: 1.5s; visibility: visible;">

        <div class="book-eye-test_cover">

          <div class="row book-eye-test_content">

            <div class="col-lg-5 col-md-12 col-sm-12 ">

              <div class="content7 slideInLeft animated" data-animate="slideInLeft" data-duration="2s" style="animation-duration: 1.5s; visibility: visible;">

                <img src="images/eye-test_icon.png" alt="">

              </div>

            </div>

            <div class="col-lg-7 col-md-12 col-sm-12">

              <div class="content7 slideInRight animated" data-animate="slideInRight" data-duration="2s" style="animation-duration: 1.5s; visibility: visible;">

                <h3>Book an eye test today.</h3>

                <p><input type="text" name="" value="" placeholder="Postal Code"><a href="">Start Now</a></p>

                <p>or Call us for appointment <b style="margin-left: 10px;font-size:17px">024 7644 2272</b></p>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<section>

  <div class="container">

    <div class="row part7">

      <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-center">

        <div class="contactlens_img">

          <img src="images/aboutus.png" alt="" class="contact_style fadeIn animated" data-animate="fadeIn" data-duration="2.5s">

          <div class="content7 fadeInUp animated" data-animate="fadeInUp" data-duration="1.5s" style="animation-duration: 1.5s; visibility: visible;">

            <h1>iCare Vision Specialists is an exclusive</br>optical showroom located in heart of</br>Conventry</h1></br>

            <a href="#">Know more<i class="fa fa-chevron-right" style="padding-left:28px;"></i></a>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<!-- horizontal line -->

<div class="horizontal_line1">

  <hr>

</div>



<section>

  <div class="container">

    <div class="row part8">

      <div class="col-lg-12 col-md-12 col-sm-12">

        <div class="text-center">

          <p>Brands we sell</p>

          <div id="mainSlider" class="owl-carousel owl-theme owl-loaded carousel1">

            <div class="item">

              <img src="images/essilor1.png" alt="">

            </div>

            <div class="item">

            <img src="images/crizal2.png" alt="">

            </div>

            <div class="item">

              <img src="images/hoya1.png" alt="">

            </div>

            <div class="item">

              <img src="images/transitions1.png" alt="">

            </div>

            <div class="item">

              <img src="images/zeiss2.png" alt="">

            </div>

            <div class="item">

              <img src="images/cibavision1.png" alt="">

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>



<!-- footer -->

<?php include 'includes/footer.php'; ?>

