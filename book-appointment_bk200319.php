<!-- header -->
<?php
$title = 'Book-appointment';
include 'includes/header.php';
?>

<!-- body -->
<!-- row1 -->
<section>
  <div class="container">
    <div class="appointment_cover_img">
      <div class="appointment_content">
        <div class="row m-0">
            <div class="col-lg-5 col-md-6 col-sm-12">
              <div class="slideInLeft animated" data-animate="slideInLeft" data-duration="2.0s" style="animation-duration: 3s; visibility: visible;">
              <p>Easy way to </br>Online Booking...</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- row2 -->
<section>
  <div class="container">
    <form action="#" method="POST">
    <div class="row form_style1">
      <!-- book an eye test -->
      <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="pulse animated" data-animate="pulse" data-duration="3.0s" style="animation-duration: 3s; visibility: visible;">
        <h4>BOOK YOUR <span style="color: #015dba;">EYE TEST</span></h4>
          <div class="row">
            <div class="col-md-6 col-sm-12">
              <input type="text" name="" value="" class="form-control" placeholder="First Name">
            </div>
            <div class="col-md-6 col-sm-12">
              <input type="text" name="" value="" class="form-control" placeholder="Surname">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <input type="tel" name="" value="" class="form-control" placeholder="Contact Number">
            </div>
            <div class="col-md-6 col-sm-12"></div>
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <input type="email" name="" value="" class="form-control" placeholder="Email Address">
            </div>
            <div class="col-md-6 col-sm-12"></div>
          </div>
          <table width="100%" border="0" class="app_contact_det" style="font-size:13px;font-weight:500;margin-top: 25px;" cellpadding="5">
            <tr>
              <th colspan="2" style="font-size: 20px;text-align: center;color: #015dba">Contact Details</th>
            </tr>
            <tr>
              <td></td>
            </tr>
            <tr>
              <td style="font-size:32px;text-align:center"><i class="fa fa-map-marker" style="color:#015dba;"></i></td>
              <td>	291 - 293 Walsgrave Rd </br>Coventry CV2 4BE</td>
            </tr>
            <tr>
              <td></td>
            </tr>
            <tr>
              <td style="font-size:26px;text-align:center"><i class="fa fa-phone" style="color:#015dba;"></i></td>
              <td>02476 442 272</td>
            </tr>
            <tr>
              <td style="font-size:26px;text-align:center"><i class="fa fa-envelope-open" style="color:#015dba;"></i></td>
              <td>info@icare-vs.co.uk</td>
            </tr>
          </table>
        </div>
        </div>
        <!-- appointment details -->
        <div class="col-lg-6 col-md-12 col-sm-12">
          <div class="pulse animated" data-animate="pulse" data-duration="3.0s" style="animation-duration: 3s; visibility: visible;">
          <h4 style="color: #015dba;">APPOINTMENT DETAILS</h4>
          <table cellpadding="8" align="center">
            <tr>
              <th>Appoinment type</th>
            </tr>
            <tr>
              <td><input type="checkbox" name="" value=""> Eye Examination</td>
            </tr>
            <tr>
              <td><input type="checkbox" name="" value=""> FREE Contact Lens Comfort Trial</td>
            </tr>
            <tr>
              <td><input type="checkbox" name="" value=""> Contact Lens Fitting/Care Appointment</td>
            </tr>
            <tr>
              <td><input type="checkbox" name="" value=""> Co-operate Eye Care Voucher</td>
            </tr>
            <tr>
              <th>Appointment 1<sup>st</sup> Choice</th>
            </tr>
            <tr>
              <td>Date  <input type="date" name="" value="" id="text-calendar" class="form-control calendar"></td>
              <td>Time  <input type="time" name="" value="" class="form-control"></td>
            </tr>
            <tr>
              <th>Appointment 2<sup>nd</sup> Choice</th>
            </tr>
            <tr>
              <td>Date  <input type="date" name="" value="" id="text-calendar" class="form-control calendar"></td>
              <td>Time  <input type="time" name="" value="" class="form-control"></td>
            </tr>
          </table>
        </div>
        </div>
      </div>
      <div class="fadeIn animated" data-animate="fadeIn" data-duration="2s" style="animation-duration: 2s; visibility: visible;">
      <div class="row" style="padding:10px;">
        <div class="col-md-12 col-sm-12 d-flex justify-content-center">
          <button type="submit" class="btn2">Submit</button>
          <button type="reset" class="btn2">Reset</button>
        </div>
      </div>
    </div>
    </form>
  </div>
</section>

<!-- footer -->
<?php include 'includes/footer.php'?>
