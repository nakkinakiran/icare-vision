<footer>
  <div class="container">
    <!-- first row -->
    <div class="row">
      <div class="col-md-3 col-sm-12">
        <ul>
          <li><h3>iCare Vision</h3></li>
          <li><a href="what-we-do.php" target="_self">What we do</a></li>
          <li><a href="#" target="_self">iCare NHS Guide</a></li>
          <li><a href="contact-us.php" target="_self">Contact us</a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-12 foot_info">
        <ul>
          <li><h3>Information</h3></li>
          <li><a href="frames.php" target="_self">Frames</a></li>
          <li><a href="#" target="_self">Contact Lenses</a></li>
          <li><a href="eye-health.php" target="_self">Eye Care Health</a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-12 foot_social">
        <ul>
          <li><h3>Get Social</h3></li>
          <li><i class="fa fa-facebook-square"></i><a href="https://www.facebook.com/" target="_blank">Facebook</a></li>
          <li><i class="fa fa-twitter-square"></i><a href="https://twitter.com/" target="_blank">Twitter</a></li>
          <li><i class="fa fa-instagram"></i><a href="https://www.instagram.com/" target="_blank">Instagram</a></li>
        </ul>
      </div>
    </div>
    <!-- second row -->
    <div class="row">
      <div class="col-lg-4 col-md-7 col-sm-12 foot_rights">
        <p>&#169; 2019 iCare Vision Specialists. All Rights Reserved</p>
      </div>
      <!-- <div class="col-md-1"></div> -->
      <div class="col-lg-3 col-md-4 col-sm-12 foot_policy">
        <p>Disclaimer . <a href="#"> Privacy Policy</a></p>
      </div>
      <div class="col-lg-5 col-md-6 col-sm-12 foot_design">
        <a href="https://www.quickinnovations.co.uk/" target="_blank"><p>Web Design by Quick Innovation</p></a>
      </div>
    </div>
  </div>
</footer>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script>
  $('.carousel1').owlCarousel({
    loop:true,
		margin:10,
		nav:false,
		dots:true,
		responsiveClass:true,
		autoplay:true,
    autoplayTimeout: 2000,
		responsive:{
      0:{
        items:1,
      },
      600:{
        items:3,
      },
      1000:{
        items:5
      }
    }
  });
  $('.testi').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:true,
		responsiveClass:true,
		// autoplay:true,
    // autoplayTimeout:3600,
    responsive:{
      0:{
        items:1,
      },
      600:{
        items:1,
      },
      1000:{
        items:1
      }
	   }
   });
   $('.carousel2').owlCarousel({
     loop:true,
    margin:10,
    nav:false,
    dots:true,
    responsiveClass:true,
    autoplay:true,
    autoplayTimeout: 2000,
    responsive:{
       0:{
         items:1,
       },
       600:{
         items:2,
       },
       1000:{
         items:3,
       }
     }
   });
</script>
<script>
  $('.animated').scrolla();
</script>

<!-- scroll style -->
<script type="text/javascript">
	$(window).scroll(function() {
		if($(document).scrollTop() > 100) {
			$('header').addClass('color-change');
		} else {
			$('header').removeClass('color-change');
		}
	});
</script>

<!-- calendar -->
<script type="text/javascript">
$(function() {
  $('input.calendar').pignoseCalendar({
    format: 'YYYY-MM-DD', // date format string. (2017-02-02)
    theme: 'light' // light, dark, blue
  });
});
</script>

<script type="text/javascript">
$('.clockpicker').clockpicker()
  .find('input').change(function(){
    console.log(this.value);
  });
var input = $('#single-input').clockpicker({
  placement: 'bottom',
  align: 'left',
  autoclose: true,
  'default': 'now'
});
var input = $('#single-input1').clockpicker({
  placement: 'bottom',
  align: 'left',
  autoclose: true,
  'default': 'now'
});

$('.clockpicker-with-callbacks').clockpicker({
    donetext: 'Done',
    init: function() {
      console.log("colorpicker initiated");
    },
    beforeShow: function() {
      console.log("before show");
    },
    afterShow: function() {
      console.log("after show");
    },
    beforeHide: function() {
      console.log("before hide");
    },
    afterHide: function() {
      console.log("after hide");
    },
    beforeHourSelect: function() {
      console.log("before hour selected");
    },
    afterHourSelect: function() {
      console.log("after hour selected");
    },
    beforeDone: function() {
      console.log("before done");
    },
    afterDone: function() {
      console.log("after done");
    }
  })
  .find('input').change(function(){
    console.log(this.value);
  });

// Manually toggle to the minutes view
$('#check-minutes').click(function(e){
  // Have to stop propagation here
  e.stopPropagation();
  input.clockpicker('show')
      .clockpicker('toggleView', 'minutes');
});
$(document).ready(function() {
    $("label").click(function() {
      var $this = $(this);                           // store jQuery object
      $this.addClass("active");                      // add to clicked item
  });
});
</script>

<!-- book appointment form validation -->
<script type="text/javascript">
  function validation(){
    var fname = document.getElementById('fname').value;
    var surname = document.getElementById('surname').value;
    var contact = document.getElementById('contact').value;
    var email = document.getElementById('email').value;
    var check1 = document.getElementById('check1');
    var check2 = document.getElementById('check2');
    var check3 = document.getElementById('check3');
    var check4 = document.getElementById('check4');
    var mobile_pattern = /^[0-9]+/;
    if(fname == ''){
      $('#fname_error').css('color','red').html('Enter your First Name');
      return false;
    }
    else{
      $('#fname_error').hide();
    }
    if(surname == ''){
      $('#surname_error').css('color','red').html('Enter your Surname');
      return false;
    }
    else{
      $('#surname_error').hide();
    }
    if(contact == ''){
      $('#contact_error').css('color','red').html('Enter your Mobile Number');
      return false;
    }
    else if(mobile_pattern.test(contact) == false){
      $('#contact_error').css('color','red').html('Enter valid Mobile Number');
      return false;
    }
    else{
      $('#contact_error').hide();
    }
    if(email == ''){
      $('#email_error').css().html('Enter your email');
      return false;
    }
    else{
      $('#email_error').hide();
    }
    if(check1.checked ==  false && check2.checked ==  false && check3.checked ==  false && check4.checked ==  false){
      $('#check1_error').css('color','red').html('Select your appointment type');
      return false;
    }
    else{
      $('#check1_error').hide();
    }
    if(fname == '' && surname == '' && contact == '' && email == '' && check1.checked == false
  ){
      alert('please fill all the fields');
    }
  }
</script>

<!-- contact form validation -->
<script type="text/javascript">
  function contact_validation(){
    var fullname = document.getElementById('fullname').value;
    var contact_number = document.getElementById('contact_number').value;
    var contact_email = document.getElementById('contact_email').value;
    var contact_querybox = document.getElementById('contact_querybox').value;
    var mobile_pattern = /^[0-9]+/;
    if(fullname == ''){
      $('#fullname_error').css('color','red').html('Enter your Full Name');
      return false;
    }
    else{
      $('#fullname_error').hide();
    }
    if(contact_number == ''){
      $('#contact_number_error').css('color','red').html('Enter your Mobile Number');
      return false;
    }
    else if(mobile_pattern.test(contact_number) == false){
      $('#contact_number_error').css('color','red').html('Enter your Valid Mobile Number');
      return false;
    }
    else{
      $('#contact_number_error').hide();
    }
    if(contact_email == ''){
      $('#contactemail_error').css('color','red').html('Enter your Email');
      return false;
    }
    else{
      $('#contactemail_error').hide();
    }
    if(contact_querybox == ''){
      $('#contact_querybox_error').css('color','red').html('Enter your Query');
      return false;
    }
    else{
      $('#contact_querybox_error').hide();
    }
  }
</script>

<!-- Eyetest Modal -->
<div class="modal fade" id="eyetest_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color: #015dba;
          color: #fff;
          border-radius: 100%;
          padding: 5px 10px;">
          <span aria-hidden="true" style="font-size: 24px;">&times;</span>
        </button>
        <br>
        <h2>Your Eye Test Explained</h2>
        <br>
        <p>To maintain good eye health and vision, it is important to have your eyes tested every two years or more frequently if advised. You should seek advice immediately if you have any sudden loss of vision, eye pain, inflammation or irritation. Most eye diseases can be found during a routine eye test and treated when detected at an early stage. If you have health problems, such as diabetes, you may need to visit your optometrist more frequently to detect any complications.</p>
        <ul>
          <li>Applanation method</li>
          <li>Corneal and retinal topography</li>
          <li>Flourescein angiogram</li>
          <li>Pupillary dilation test</li>
          <li>Refraction test</li>
          <li>Slit-lamp exam</li>
          <li>Tonometry</li>
          <li>Ultrsound</li>
          <li>Visual acuity testing</li>
          <li>Visual field test</li>
        </ul>
        <button href="" type="submit" class="btn4 offset-md-3" name="submit" id="" value="submit" onclick="openEyetest()" data-target="#myslide" data-dismiss="modal">Book an Eyetest<i class="fa fa-arrow-right" style="padding-left:30px;"></i></button>
      </div>
    </div>
  </div>
</div>

<!-- Care for your Eye Modal -->
<div class="modal fade" id="eyecare_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <!-- <form class="" action="" method="post"> -->
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color: #015dba;
            color: #fff;
            border-radius: 100%;
            padding: 5px 10px;">
            <span aria-hidden="true" style="font-size: 24px;">&times;</span>
          </button>
          <br>
          <h2>Taking Care of your Eyes</h2>
          <br>
          <p>Eye care is an essential part of any healthy lifestyle. Regular exercise, eating a balanced diet, watching your weight and making sure you have your eyes checked regularly doesn't just keep your vision in tip-top condition, but these measures can also help in the detection of long-term health issues which can lead to permanent eye damage.</p>
          <p>The following are the measures that need to be taken during travel time</p>
          <div class="row">
            <div class="col-lg-12">
              <ul>
                <li>Travel with contact lenses</li>
                <li>Protect your eyes from the sun</li>
                <li>Keep your eyes comfortable when you travel</li>
                <li>Avoid eye infections</li>
                <li>Give your eyes a holiday</li>
              </ul>
            </div>
            <!--<div class="col-lg-6">-->
            <!--  <img src="images/eyeball.png" alt="">-->
            <!--</div>-->
          </div>
          <button href="" type="submit" class="btn4 offset-md-3" name="submit" id="" value="submit" onclick="openEyetest()" data-target="#myslide" data-dismiss="modal">Book an Eyetest<i class="fa fa-arrow-right" style="padding-left:30px;"></i></button>
        <!-- </form> -->
      </div>
    </div>
  </div>
</div>

<!-- Children Eye Test Modal -->
<div class="modal fade" id="children_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color: #015dba;
          color: #fff;
          border-radius: 100%;
          padding: 5px 10px;">
          <span aria-hidden="true" style="font-size: 24px;">&times;</span>
        </button>
        <br>
        <h2>Children Eye Test</h2>
        <br>
        <p>A child's vision usually keeps on developing up until the age of seven or eight. It's often far easier for us to detect any eye problems. And it's important that any problems are identified as early as possible because they can have an impact on a child's development and education.</p>
        <p>Eighty percent of what a child learns is via their vision. And any problems which remain untreated could see them falling behind in class, perhaps leading to emotional problems and even a misdiagnosis of dyslexia or another cognitive disorder.</p>
        <h6>Spotting the signs</h6>
        <p>A report by the association of optometrists has highlighted the fact that one in five children have an undetected eye problem.</p>
        <p>This is why it is so important to get your child tested early, so that any potential problems can be identified and managed. However, there are signs you can look out of yourself.</p>
        <ul>
          <li>Frequent squinting or frowning</li>
          <li>Excessive eye rubbing</li>
          <li>Closing one eye when watching TV or reading</li>
          <li>Tired eyes or headaches</li>
          <li>Skipping words or whole lines when reading</li>
          <li>Difficulty seeing text from a board</li>
          <li>Poor literacy</li>
          <li>Sensitivity to light</li>
        </ul>
        <button href="" type="submit" class="btn4 offset-md-3" name="submit" id="" value="submit" onclick="openEyetest()" data-target="#myslide" data-dismiss="modal">Book an Eyetest<i class="fa fa-arrow-right" style="padding-left:30px;"></i></button>
      </div>
    </div>
  </div>
</div>

<!-- Eye Service Modal -->
<div class="modal fade" id="eyeservice_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color: #015dba;
          color: #fff;
          border-radius: 100%;
          padding: 5px 10px;">
          <span aria-hidden="true" style="font-size: 24px;">&times;</span>
        </button>
        <br>
        <h2>Eye Health Services</h2>
        <br>
        <p>Eyesight is your window to the outside world. And your inner world too. What you see inspires your emotions. It forms your memories and supplies the raw materials of your imagination.</p>
        <p>There are simple steps you can take to protect and care for your eyes, starting with an Eye Test.</p>
        <h4>Your Eye Test</h4>
        <p>Our optometrists have over 100 ways of assessing your vision and eye health. And they will select the tests most appropriate to you. Although every eye test is different, you can expect to be with our eye care professionals for about 30 minutes.</p>
        <h4>Children's eye tests</h4>
        <p>It’s important to get kids tested early, so that any potential problems can be identified. Although no less thorough than an adult eye test, we’ve made the assessment of a child’s eye heath and vision much more fun.</p>
        <h4>Eye conditions</h4>
        <p>By familiarising yourself with the symptoms of certain conditions, you could prevent an initially minor infection or problem becoming a major health issue.</p>
        <h4>Caring for your eyes</h4>
        <p>Most of us are aware of the benefits of regular exercise and a balanced diet. But eye health is an often-overlooked aspect of our wellbeing. If cared for properly, your eyesight could last a lifetime. Which is why it’s important you know how to look after and protect your eyes.</p>
        <button href="" type="submit" class="btn4 offset-md-3" name="submit" id="" value="submit" onclick="openEyetest()" data-target="#myslide" data-dismiss="modal">Book an Eyetest<i class="fa fa-arrow-right" style="padding-left:30px;"></i></button>
      </div>
    </div>
  </div>
</div>

<!-- Eye Conditions Modal -->
<div class="modal fade" id="eyecondition_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background-color: #015dba;
          color: #fff;
          border-radius: 100%;
          padding: 5px 10px;">
          <span aria-hidden="true" style="font-size: 24px;">&times;</span>
        </button>
        <br>
        <h2>Common Eye Contitions</h2>
        <br>
        <p>Below you'll find summaries of common eye and vision conditions. These are just a few of the common eye conditions that can effect almost anyone, but any injury to any part of the eye area or eye-related condition should be checked at our store to prevent any longterm damage.</p>
        <ul>
          <li>Astigmatism</li>
          <li>Cataracts</li>
          <li>Conjunctivitis</li>
          <li>Diabetes</li>
          <li>Dry Eye</li>
        </ul>
        <table width="100%">
          <tr>
            <td><h5>Cataract</h5></td>
            <td id="arrow_style"><a id="cat_btn"><i class="fa fa-angle-down"></i></a></td>
            </tr>
            <tr>
              <td><p id="cat_content">A cataract is a clouding that develops in the crystalline lens of the eye or in its envelope, varying in degree from slight to complete opacity and obstructing the passage of light. Early in the developement of age-related cataract, the power of the lens may be increased, causing near-sightedness(myopia), and the gradual yellowing and opacification of the lens may reduce the perception of blue colors, cataracts typically progress slowly to cause vision loss, and are potentially blinding if untreated. The condition usually affects both eyes, but almost always one eye is affected earlier than the other.</p></td>
            </tr>
            <tr>
              <td><h5>Dry Eye</h5></td>
              <td id="arrow_style"><a id="dry_btn"><i class="fa fa-angle-down"></i></a></td>
            </tr>
            <tr>
              <td id="dry_content"><p>Dry eye is a condition caused by the eyes not producing enough tears, of a poor quality, leading to a feeling of gritty uncomfortable eyes, some eyes produce too many tears to compensate and become watery, Dry eye can be a symptom of underlying health issues, including:</p>
                <ul>
                  <li>The natural aging process, especially during the female menopause.</li>
                  <li>Side effects of using certain medications such as antihistamines and birth control pills.</li>
                  <li>Diseases that affect the ability to make tears, such as Sjogren's syndrome, rheumatoid arthritis and collagen vascular diseases.</li>
                  <li>Structural problems with the eyes or a problem with the tear ducts.</li>
                </ul>
                <p>Dry eye can usually be treated by artificical tear substitutes, ask your optician which would be best for you.</p>
              </td>
            </tr>
            <tr>
              <td><h5>Macula Degeneration</h5></td>
              <td id="arrow_style"><a id="macula_btn"><i class="fa fa-angle-down"></i></a></td>
            </tr>
            <tr>
              <td id="macula_content">
                <p>Age-related macular degeneration is a medical condition which usually affects older adults and results in a loss of vision in the centre of the visual field(the macula) because of damage to the retina. It occurs in "dry" and "wet" forms. It is a major cause of blindness and visual impairment in older adults (>50  years). Macular degeneration can make it difficult or impossible to read or recognize faces, although enough peripheral vision remains to allow other activites of daily life.</p>
              </td>
            </tr>
            <tr>
              <td><h5>Spots and Floaters</h5></td>
              <td id="arrow_style"><a id="spots_btn"><i class="fa fa-angle-down"></i></a></td>
            </tr>
            <tr>
              <td id="spots_content">
                <p>Most people notice spots or floaters of their eyes. They are especially noticeable when looking at a plain white background and appear as greyish specks in our vision.</p>
                <p>The vitreous fluid, the jelly inside the eye, contains small particles. These cast a shadow onto the retina at the back of the eye, which appear as spots and floaters.</p>
                <p>If there is a sudden increase in spots or floaters, especially if these are accompained by flashing lights, or a custain effect shadowing your vision, this may indicate a more serious problem such as Retina detachment which left untreated can cause blindness. Anyone experiencing these sudden symptoms is advised to visit their  optician immediately.</p>
              </td>
            </tr>
            <tr>
              <td><h5>Retinal Detachment</h5></td>
              <td id="arrow_style"><a id="retinal_btn"><i class="fa fa-angle-down"></i></a></td>
            </tr>
            <tr>
              <td id="retinal_content">
                <p>Retinal detachment is a disorder of the eye in which the retina peels away from its underlying layer of support tissue. Initial detachment may be localized, but without rapid treatment the entire retina may detach, leading to vision loss and blindness. It is a medical emergency.</p>
              </td>
            </tr>
            <tr>
              <td><h5>Glaucoma</h5></td>
              <td id="arrow_style"><a id="glaucoma_btn"><i class="fa fa-angle-down"></i></a></td>
            </tr>
            <tr>
              <td id="glaucoma_content">
                <p>Glaucoma is an eye disorder in which the optic nerve suffers damage, permanently damaging vision in the affected eye(s) and progressing to complete blindness. If untreated. It is often, but not always, associated with increased pressure of the fluid in the eye (aqueous humour).[1] The term 'ocular hypertension' is used for causes having constantly raised intraocular pressure (IOP) without any associated optic nerve damage. Conversely the term 'normal' or 'low tension glaucoma' is suggested for the typical visual field defects when associated with a normal or low IOP.</p>
              </td>
            </tr>
            <tr>
              <td><h5>Myopia</h5></td>
              <td id="arrow_style"><a id="myopia_btn"><i class="fa fa-angle-down"></i></a></td>
            </tr>
            <tr>
              <td id="myopia_content">
                <p>Myopia is commonly known as being nearsighted (American) and shortsighted (British). A condition of the eye where the light that comes in does not directly focus on the retina but in front of it. This causes the image that one sees when looking at a distant object to be out of focus but in foucs when looking at a close of object. Below is the image to be used for myopia.</p>
              </td>
            </tr>
            <tr>
              <td><h5>Astigmatism</h5></td>
              <td id="arrow_style"><a id="astigmatism_btn"><i class="fa fa-angle-down"></i></a></td>
            </tr>
            <tr>
              <td id="astigmatism_content">
                <p>Astigmatism is the optical term for more than one point of foucs. It occurs when the surface of the cornea or crystalline lens is not spherical. Light from an object does not focus exactly on the retina but at two seperate points. An astigmatic eye has curves that are steeper in one direction than the other. An example of this could be where the cornea is not spherical and shaped more like a Rugby ball than football - ofcourse this is not noticeable by just looking at someone's eyes.</p>
              </td>
            </tr>
        </table>
        <button href="" type="submit" class="btn4 offset-md-3" name="submit" id="" value="submit" onclick="openEyetest()" data-target="#myslide" data-dismiss="modal">Book an Eyetest<i class="fa fa-arrow-right" style="padding-left:30px;"></i></button>
      </div>
    </div>
  </div>
</div>

<!-- paragraph toggle button -->
<script>
$(document).ready(function(){
  $('#cat_content').hide();
  $('#dry_content').hide();
  $('#macula_content').hide();
  $('#spots_content').hide();
  $('#retinal_content').hide();
  $('#glaucoma_content').hide();
  $('#myopia_content').hide();
  $('#astigmatism_content').hide();
    $("#cat_btn").click(function(){
      $("#cat_content").toggle(1000);
      $("i", this).toggleClass("fa-angle-up fa-angle-down");
    });
    $("#dry_btn").click(function(){
      $("#dry_content").toggle(1000);
      $("i", this).toggleClass("fa-angle-up fa-angle-down");
    });
    $("#macula_btn").click(function(){
      $("#macula_content").toggle(1000);
      $("i", this).toggleClass("fa-angle-up fa-angle-down");
    });
    $("#spots_btn").click(function(){
      $("#spots_content").toggle(1000);
      $("i", this).toggleClass("fa-angle-up fa-angle-down");
    });
    $("#retinal_btn").click(function(){
      $("#retinal_content").toggle(1000);
      $("i", this).toggleClass("fa-angle-up fa-angle-down");
    });
    $("#glaucoma_btn").click(function(){
      $("#glaucoma_content").toggle(1000);
      $("i", this).toggleClass("fa-angle-up fa-angle-down");
    });
    $("#myopia_btn").click(function(){
      $("#myopia_content").toggle(1000);
      $("i", this).toggleClass("fa-angle-up fa-angle-down");
    });
    $("#astigmatism_btn").click(function(){
      $("#astigmatism_content").toggle(1000);
      $("i", this).toggleClass("fa-angle-up fa-angle-down");
    });
});
</script>
